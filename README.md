Rust implementation of certain OpenCL micro-benchmarks.

## Micro-Benchmarks

## How to run

### Project Dependencies
1. Platform with a CPU and a GPU
2. OpenCL 1.2 and above libraries.
3. ICD loader

### Installation
1. Rust supported platform. Check if Rust is supported for your platform [here](https://doc.rust-lang.org/book/getting-started.html#platform-support).
2. Install rust using [rustup](https://www.rustup.rs)
3. Clone the project `git clone https://gitlab.com/skanur/ubench_rust.git` or from its mirror `git clone https://git.it.abo.fi/skanur/ubench_rust.git`

### Run the project
1. Navigate to the project
2. run `cargo run --release` to run all the configured experiments

### To run tests
1. export `RUST_TEST_THREADS=1` to your environment. As the tests use OpenCL framework to test the experiments, the environment variable ensures that they are run one after the other without causing contention to the underlying device.
2. run `cargo test`

### Automated measurements of experiments on your platform
The project comes with scripts used to run measurements on the supported machines. 

#### Running the scripts
The experiment file needs administrator password stored in a plain text file. *This is a security risk, hence run this only on trusted machines!* The file named `password` should be in project root directory. Once password file exists, the experiments can be run by 

```bash
$ cd $PROJDIR/scripts
$ ./experiments
```

Note that its imperative that `experiments` is called *after* navigating to the `scripts` directory. This is because many of the environment variables are set relative to this path.

#### Machine specific configurations
Machine specific configurations are performed by editing two files - `scripts/config` and `scripts/experiments`. The machine name can be obtained by `uname -n` on a linux operating system. We first begin by editing `scripts/config`.  

The machine name is a function that must be added in `scripts/config` along with platform and device id. One can find the platform and device id using a generic OpenCL program such as [clinfo](https://github.com/Oblomov/clinfo). `clinfo` lists all available platforms and devices and the id starts with 0. Thus an OpenCL compatible device appearing second in the device list on a platform that itself appears first on the list of available platforms would have PLATFORM=0 and DEVICE=1. 

The variable `DISABLE_FREQ` is used if you want to disable setting of frequencies. This is useful for development machines where setting of frequencies are undesirable. 
The number of cores is a list of cores that are kept on while running the experiments. List all the cores that needs to be on. On the other hand, if you want a certain core turned off, don't add it to the list. Keep in mind that certain platforms require that core 0 is never turned off. 

The variable `FREQ` is used to set the frequency. This variable makes sense when `DISABLE_FREQ` is set to `false`. Make sure the frequency being used here is a valid frequency. The frequency is set using `cpupower` utility. If your platform requires a custom script, encapsulate it in a script file and then include it in `scripts/experiment` under `setCore()` and `resetCores()` functions. Check the configuration for `odroid` machine as an example of this.

Once `script/config` is complete, add your machine as a case in the function `selectConfig()` of `script/experiments`. This will make sure your machine specific configuration is chosen when the scripts are run.

*Please note that these instructions are for Linux only. Windows or other operating systems will **not** be supported by me*

Finally, experiment related configuration happen in `src/main.rs`. Refer the documentation for more details.

#### Reading result files

The per device experiment specific result files are stored in `$PROJDIR/results/${exp_name)_${device}_${date_time}.csv`. The log files consisting of build information and stderr information is stored in `$PROJDIR/scripts/${exp_name}_${date_time}.csv`.

### Measure Memory Warp Parallelism (MWP)

MWPs was determined in the [original work](http://dl.acm.org/citation.cfm?id=1555775) by examining the memory access patterns of a GPU that does not consist a cache hierarchy. This assumption is not compatible with modern GPUs that consists of atleast two levels of cache. Instead, we determine the MWP using a microbenchmark. OpenCL framework allows synchronization of workitems within a boundary of a workgroup. GPUs achieve this by scheduling the entire workgroup on a single CU. We utilize this property and design a computation kernel that consists of pure memory operations. In a device that exhibits MWP, the device achieves maximum bandwidth when there sufficient workgroups to engage all parallel memory paths. A plot of global memory throughput for increasing number of workgroup will reveal a sawtooth like pattern with a period of MWP. 

