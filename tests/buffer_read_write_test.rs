extern crate ocl;
extern crate ubench_rust;

#[cfg(test)]
mod buff_read_write_setup_3_tests {
    use ocl;
    use ubench_rust::common;
    use ubench_rust::buff_read_write;
    use ubench_rust::exp;

    #[test]
    fn test_defaults() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let buffer_read_write: buff_read_write::BufferReadWrite<f32> =
            buff_read_write::BufferReadWrite::new()
                .build()
                .expect("Construction of Startup experiment failed");

        let mut buff_exp = exp::ExpType1::new(buffer_read_write)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = buff_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }

    #[test]
    fn test_small_setup() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let incr_buffersize = vec![1];
        let buffer_read_write: buff_read_write::BufferReadWrite<f32> =
            buff_read_write::BufferReadWrite::new()
                .set_incr(incr_buffersize)
                .and_then(|m| m.build())
                .expect("Construction of Startup experiment failed");

        let mut buff_exp = exp::ExpType1::new(buffer_read_write)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = buff_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }

    #[test]
    fn int_values() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let incr_buffersize = vec![1];
        let buffer_read_write: buff_read_write::BufferReadWrite<u32> =
            buff_read_write::BufferReadWrite::new()
                .set_incr(incr_buffersize)
                .and_then(|m| m.build())
                .expect("Construction of Startup experiment failed");

        let mut buff_exp = exp::ExpType1::new(buffer_read_write)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = buff_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }

    #[test]
    fn vector_types_values() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let incr_buffersize = vec![1];
        let buffer_read_write: buff_read_write::BufferReadWrite<ocl::aliases::ClInt2> =
            buff_read_write::BufferReadWrite::new()
                .set_incr(incr_buffersize)
                .and_then(|m| m.build())
                .expect("Construction of Startup experiment failed");

        let mut buff_exp = exp::ExpType1::new(buffer_read_write)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = buff_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }
}

#[cfg(test)]
mod buff_read_write_setup_2_tests {
    use ocl;
    use ubench_rust::common;
    use ubench_rust::buff_read_write;
    use ubench_rust::traits::GetDimensions;
    use ubench_rust::traits::SetDimensionsParam;
    use ubench_rust::traits::HasKernel;

    #[test]
    fn buffer_size_must_be_equal_to_workitems_for_scalar_types() {
        let common::SetupTest { platform_id, device_id, .. } = common::setup_test();
        let incr_buffersize = vec![1];
        let mut buffer_read_write: buff_read_write::BufferReadWrite<i64> =
            buff_read_write::BufferReadWrite::new()
                .set_incr(incr_buffersize.clone())
                .and_then(|m| m.build())
                .expect("Construction of Startup experiment failed");

        let plt = ocl::Platform::list()[platform_id];
        let dev = ocl::Device::list_all(&plt).unwrap()[device_id];
        let context = ocl::Context::builder()
            .platform(plt)
            .devices(dev)
            .build()
            .unwrap();
        let queue = ocl::Queue::new(&context, dev).unwrap();
        let program = ocl::Program::builder()
            .src(buffer_read_write.create_kernel())
            .devices(dev)
            .build(&context)
            .unwrap();

        // Iterate to last, but don't consume the iterator
        let mut param = buffer_read_write.next();
        while param.is_some() {
            param = buffer_read_write.next();
            buffer_read_write.set_global_workdimensions_param(&dev, &queue, &program);
        }

        let gsize = buffer_read_write.get_global_workdimensions()[0];
        let max_workitems: usize = (incr_buffersize[0] * 1024 * 1024) as usize;
        assert!(gsize == max_workitems,
                "gsize = {} and max_workitems = {}",
                gsize,
                max_workitems);
    }

    #[test]
    fn buffer_size_must_be_equal_workitems_for_vector_types() {
        let common::SetupTest { platform_id, device_id, .. } = common::setup_test();
        let incr_buffersize = vec![1];
        let mut buffer_read_write: buff_read_write::BufferReadWrite<ocl::aliases::ClInt2> =
            buff_read_write::BufferReadWrite::new()
                .set_incr(incr_buffersize.clone())
                .and_then(|m| m.build())
                .expect("Construction of Startup experiment failed");

        let plt = ocl::Platform::list()[platform_id];
        let dev = ocl::Device::list_all(&plt).unwrap()[device_id];
        let context = ocl::Context::builder()
            .platform(plt)
            .devices(dev)
            .build()
            .unwrap();
        let queue = ocl::Queue::new(&context, dev).unwrap();
        let program = ocl::Program::builder()
            .src(buffer_read_write.create_kernel())
            .devices(dev)
            .build(&context)
            .unwrap();

        // Iterate to last, but don't consume the iterator
        let mut param = buffer_read_write.next();
        while param.is_some() {
            param = buffer_read_write.next();
            buffer_read_write.set_global_workdimensions_param(&dev, &queue, &program);
        }

        let gsize = buffer_read_write.get_global_workdimensions()[0];
        let max_workitems: usize = (1 * 1024 * 1024 / 2) as usize;
        assert!(gsize == max_workitems,
                "gsize = {} and max_workitems = {}",
                gsize,
                max_workitems);
    }
}

#[cfg(test)]
mod buffer_read_write_no_setup {
    use ubench_rust::buff_read_write;

    #[test]
    #[should_panic]
    fn bad_experiment_name_should_fail() {
        buff_read_write::BufferReadWrite::<f64>::new()
            .set_experiment_name("bad;bad_name")
            .and_then(|m| m.build())
            .expect("Construction of Startup experiment failed");
    }

}
