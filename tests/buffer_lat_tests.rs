extern crate ocl;
extern crate ubench_rust;

#[cfg(test)]
mod mem_access_test {
    use ocl;
    use ubench_rust::buff_lat;
    use ubench_rust::common;
    use ubench_rust::ocltypes::HasWidth;

    #[test]
    fn test_mem_location_scalar() {
        let common::SetupTest { platform_id, device_id, .. } = common::setup_test();
        let unroll_factor = 1000;
        let step = 1;
        let buff_lat: buff_lat::BufferLat<u64> = buff_lat::BufferLat::new()
            .set_max_loops(unroll_factor, step, platform_id, device_id)
            .and_then(|m| m.build())
            .unwrap();
        let max_mem_alloc = buff_lat.get_max_mem_alloc(platform_id, device_id);
        println!("{}, {}, {}, {}",
                 step,
                 unroll_factor,
                 buff_lat.get_loops(),
                 max_mem_alloc);

        for i in 1..buff_lat.get_loops() {
            for u in 0..unroll_factor {
                assert!(u * step * i < max_mem_alloc,
                        format!("Assertion failed @ {}, {}, {}, {}",
                                i,
                                u,
                                step,
                                max_mem_alloc));
            }
        }
    }

    #[test]
    fn test_mem_location_vector() {
        let common::SetupTest { platform_id, device_id, .. } = common::setup_test();
        let unroll_factor = 1000;
        let step = 1;
        let buff_lat: buff_lat::BufferLat<ocl::aliases::ClFloat16> = buff_lat::BufferLat::new()
            .set_max_loops(unroll_factor, step, platform_id, device_id)
            .and_then(|m| m.build())
            .unwrap();
        let max_mem_alloc = buff_lat.get_max_mem_alloc(platform_id, device_id);
        println!("{}, {}, {}, {}",
                 step,
                 unroll_factor,
                 buff_lat.get_loops(),
                 max_mem_alloc);
        let ocl_type: ocl::aliases::ClFloat16 = Default::default();
        let element_vector_width: u64 = ocl_type.get_width() as u64;

        for i in 1..buff_lat.get_loops() {
            for u in 0..unroll_factor {
                assert!(((u * (step * element_vector_width)) * i) < max_mem_alloc,
                        format!("Assertion failed @ {}, {}, {}, {}",
                                i,
                                u,
                                step,
                                max_mem_alloc));
            }
        }
    }
}

#[cfg(test)]
mod buffer_lat_max_values {
    use ocl;
    use ubench_rust::buff_lat;
    use ubench_rust::common;
    use ubench_rust::traits::SetDimensionsParam;
    use ubench_rust::traits::HasKernel;

    #[test]
    #[should_panic]
    fn test_max_loop_value_for_scalar() {
        let common::SetupTest { platform_id, device_id, .. } = common::setup_test();
        let unroll_factor = 10;
        let step = 1024;
        let mut buff_lat: buff_lat::BufferLat<f32> = buff_lat::BufferLat::new()
            .set_max_loops(unroll_factor, step, platform_id, device_id)
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");
        let max_loops = buff_lat.get_loops() + 2;
        buff_lat.set_loops(max_loops)
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");

        let plt = ocl::Platform::list()[platform_id];
        let dev = ocl::Device::list_all(&plt).unwrap()[device_id];
        let context = ocl::Context::builder()
            .platform(plt)
            .devices(dev)
            .build()
            .unwrap();
        let queue = ocl::Queue::new(&context, dev).unwrap();
        let program = ocl::Program::builder()
            .src(buff_lat.create_kernel())
            .devices(dev)
            .build(&context)
            .unwrap();

        // This should panic!
        buff_lat.set_global_workdimensions_param(&dev, &queue, &program);
    }

    #[test]
    #[should_panic]
    fn test_max_loop_value_for_vector() {
        let common::SetupTest { platform_id, device_id, .. } = common::setup_test();
        let unroll_factor = 10;
        let step = 1024;
        let mut buff_lat: buff_lat::BufferLat<ocl::aliases::ClFloat16> = buff_lat::BufferLat::new()
            .set_max_loops(unroll_factor, step, platform_id, device_id)
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");
        let max_loops = buff_lat.get_loops() + 2;
        buff_lat.set_loops(max_loops)
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");

        let plt = ocl::Platform::list()[platform_id];
        let dev = ocl::Device::list_all(&plt).unwrap()[device_id];
        let context = ocl::Context::builder()
            .platform(plt)
            .devices(dev)
            .build()
            .unwrap();
        let queue = ocl::Queue::new(&context, dev).unwrap();
        let program = ocl::Program::builder()
            .src(buff_lat.create_kernel())
            .devices(dev)
            .build(&context)
            .unwrap();

        // This should panic!
        buff_lat.set_global_workdimensions_param(&dev, &queue, &program);
    }

    #[test]
    #[should_panic]
    fn test_max_unroll_value_for_scalar() {
        let common::SetupTest { platform_id, device_id, .. } = common::setup_test();
        let loops = 10;
        let step = 1024;
        let mut buff_lat: buff_lat::BufferLat<f32> = buff_lat::BufferLat::new()
            .set_max_unroll_factor(loops, step, platform_id, device_id)
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");
        let max_unroll_factor = buff_lat.get_unroll_factor() + 2;
        buff_lat.set_unroll_factor(max_unroll_factor)
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");

        let plt = ocl::Platform::list()[platform_id];
        let dev = ocl::Device::list_all(&plt).unwrap()[device_id];
        let context = ocl::Context::builder()
            .platform(plt)
            .devices(dev)
            .build()
            .unwrap();
        let queue = ocl::Queue::new(&context, dev).unwrap();
        let program = ocl::Program::builder()
            .src(buff_lat.create_kernel())
            .devices(dev)
            .build(&context)
            .unwrap();

        // This should panic!
        buff_lat.set_global_workdimensions_param(&dev, &queue, &program);
    }

    #[test]
    #[should_panic]
    fn test_max_unroll_value_for_vector() {
        let common::SetupTest { platform_id, device_id, .. } = common::setup_test();
        let loops = 10;
        let step = 1024;
        let mut buff_lat: buff_lat::BufferLat<ocl::aliases::ClFloat16> = buff_lat::BufferLat::new()
            .set_max_unroll_factor(loops, step, platform_id, device_id)
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");
        let max_unroll_factor = buff_lat.get_unroll_factor() + 2;
        buff_lat.set_unroll_factor(max_unroll_factor)
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");

        let plt = ocl::Platform::list()[platform_id];
        let dev = ocl::Device::list_all(&plt).unwrap()[device_id];
        let context = ocl::Context::builder()
            .platform(plt)
            .devices(dev)
            .build()
            .unwrap();
        let queue = ocl::Queue::new(&context, dev).unwrap();
        let program = ocl::Program::builder()
            .src(buff_lat.create_kernel())
            .devices(dev)
            .build(&context)
            .unwrap();

        // This should panic!
        buff_lat.set_global_workdimensions_param(&dev, &queue, &program);
    }
}

#[cfg(test)]
mod buffer_lat_exp_test {
    use ocl;
    use ubench_rust::buff_lat;
    use ubench_rust::exp;
    use ubench_rust::common;

    #[test]
    fn test_defaults() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let buff_lat: buff_lat::BufferLat<f32> = buff_lat::BufferLat::new()
            .build()
            .expect("Construction of BufferLat experiment failed");

        let mut buff_exp = exp::ExpType1::new(buff_lat)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = buff_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }

    #[test]
    fn test_small_setup() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let flags = vec![buff_lat::ReadWriteFlags {
                             read_flag: ocl::flags::MEM_READ_WRITE,
                             write_flag: ocl::flags::MEM_READ_WRITE,
                         },
                         buff_lat::ReadWriteFlags {
                             read_flag: ocl::flags::MEM_USE_HOST_PTR,
                             write_flag: ocl::flags::MEM_USE_HOST_PTR,
                         }];
        let buff_lat: buff_lat::BufferLat<f32> = buff_lat::BufferLat::new()
            .set_flags(&flags)
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");

        let mut buff_exp = exp::ExpType1::new(buff_lat)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = buff_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }

    #[test]
    fn int_values() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let flags = vec![buff_lat::ReadWriteFlags {
                             read_flag: ocl::flags::MEM_READ_WRITE,
                             write_flag: ocl::flags::MEM_READ_WRITE,
                         },
                         buff_lat::ReadWriteFlags {
                             read_flag: ocl::flags::MEM_USE_HOST_PTR,
                             write_flag: ocl::flags::MEM_USE_HOST_PTR,
                         }];
        let buff_lat: buff_lat::BufferLat<u32> = buff_lat::BufferLat::new()
            .set_flags(&flags)
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");

        let mut buff_exp = exp::ExpType1::new(buff_lat)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = buff_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }

    #[test]
    fn vector_types_values() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let flags = vec![buff_lat::ReadWriteFlags {
                             read_flag: ocl::flags::MEM_READ_WRITE,
                             write_flag: ocl::flags::MEM_READ_WRITE,
                         },
                         buff_lat::ReadWriteFlags {
                             read_flag: ocl::flags::MEM_USE_HOST_PTR,
                             write_flag: ocl::flags::MEM_USE_HOST_PTR,
                         }];
        let buff_lat: buff_lat::BufferLat<ocl::aliases::ClInt16> = buff_lat::BufferLat::new()
            .set_flags(&flags)
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");

        let mut buff_exp = exp::ExpType1::new(buff_lat)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = buff_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }
}

#[cfg(test)]
mod buffer_lat_with_host_memory {
    use ocl;
    use ubench_rust::buff_lat;
    use ubench_rust::exp;
    use ubench_rust::common;

    #[test]
    fn scalar_values_as_mem_alloc_host_ptr() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let flags = vec![buff_lat::ReadWriteFlags {
                             read_flag: ocl::flags::MEM_ALLOC_HOST_PTR,
                             write_flag: ocl::flags::MEM_ALLOC_HOST_PTR,
                         }];
        let buff_lat: buff_lat::BufferLat<f32> = buff_lat::BufferLat::new()
            .set_flags(&flags)
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");

        let mut buff_exp = exp::ExpType1::new(buff_lat)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = buff_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }

    #[test]
    fn vector_values_as_mem_alloc_host_ptr() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let flags = vec![buff_lat::ReadWriteFlags {
                             read_flag: ocl::flags::MEM_ALLOC_HOST_PTR,
                             write_flag: ocl::flags::MEM_ALLOC_HOST_PTR,
                         }];
        let buff_lat: buff_lat::BufferLat<ocl::aliases::ClInt16> = buff_lat::BufferLat::new()
            .set_flags(&flags)
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");

        let mut buff_exp = exp::ExpType1::new(buff_lat)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = buff_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }

    #[test]
    fn scalar_values_as_mem_alloc_host_ptr_and_mem_copy_host_ptr() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let flags =
            vec![buff_lat::ReadWriteFlags {
                     read_flag: ocl::flags::MEM_ALLOC_HOST_PTR | ocl::flags::MEM_COPY_HOST_PTR,
                     write_flag: ocl::flags::MEM_ALLOC_HOST_PTR | ocl::flags::MEM_COPY_HOST_PTR,
                 }];
        let buff_lat: buff_lat::BufferLat<f32> = buff_lat::BufferLat::new()
            .set_flags(&flags)
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");

        let mut buff_exp = exp::ExpType1::new(buff_lat)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = buff_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }

    #[test]
    fn vector_values_as_mem_alloc_host_ptr_and_mem_copy_host_ptr() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let flags =
            vec![buff_lat::ReadWriteFlags {
                     read_flag: ocl::flags::MEM_ALLOC_HOST_PTR | ocl::flags::MEM_COPY_HOST_PTR,
                     write_flag: ocl::flags::MEM_ALLOC_HOST_PTR | ocl::flags::MEM_COPY_HOST_PTR,
                 }];
        let buff_lat: buff_lat::BufferLat<ocl::aliases::ClInt16> = buff_lat::BufferLat::new()
            .set_flags(&flags)
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");

        let mut buff_exp = exp::ExpType1::new(buff_lat)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = buff_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }
}

#[cfg(test)]
mod buffer_lat_max_values_run {
    use ocl;
    use ubench_rust::buff_lat;
    use ubench_rust::exp;
    use ubench_rust::common;

    #[test]
    fn test_max_loops_for_a_scalar_value() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let unroll_factor = 100;
        let step = 32 * 1024;
        let buff_lat: buff_lat::BufferLat<f32> = buff_lat::BufferLat::new()
            .set_max_loops(unroll_factor, step, platform_id, device_id)
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");

        println!("Maximum loops: {}", buff_lat.get_loops());

        let mut buff_exp = exp::ExpType1::new(buff_lat)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = buff_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }

    #[test]
    fn test_max_unroll_factor_for_a_scalar_value() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let loops = 100;
        let step = 32 * 1024;
        let buff_lat: buff_lat::BufferLat<f32> = buff_lat::BufferLat::new()
            .set_max_unroll_factor(loops, step, platform_id, device_id)
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");

        println!("Maximum unroll_factor: {}", buff_lat.get_unroll_factor());

        let mut buff_exp = exp::ExpType1::new(buff_lat)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = buff_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }

    #[test]
    fn test_max_loop_for_a_vector_value() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let unroll_factor = 10;
        let step = 32 * 1024;
        let buff_lat: buff_lat::BufferLat<ocl::aliases::ClFloat16> = buff_lat::BufferLat::new()
            .set_max_loops(unroll_factor, step, platform_id, device_id)
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");

        println!("Maximum loops: {}", buff_lat.get_loops());

        let mut buff_exp = exp::ExpType1::new(buff_lat)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = buff_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }

    #[test]
    fn test_max_unroll_factor_for_a_vector_value() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let loops = 10;
        let step = 32 * 1024;
        let buff_lat: buff_lat::BufferLat<ocl::aliases::ClFloat16> = buff_lat::BufferLat::new()
            .set_max_unroll_factor(loops, step, platform_id, device_id)
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");

        println!("Maximum unroll_factor: {}", buff_lat.get_unroll_factor());

        let mut buff_exp = exp::ExpType1::new(buff_lat)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = buff_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }
}

#[cfg(test)]
mod buffer_lat_no_setup {
    use ocl;
    use ubench_rust::buff_lat;
    use ubench_rust::traits::HasKernel;

    #[test]
    fn print_scalar_kernel() {
        let buff_lat = buff_lat::BufferLat::<f64>::new()
            .set_unroll_factor(2_u64)
            .and_then(|m| m.set_step(32 * 1024))
            .and_then(|m| m.set_experiment_name("test"))
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");
        let kernel = format!("
#define STEP (32768)

 __kernel void testdouble (__global double* in, __global double* out) {{
    ulong gid = get_global_id(0);
    ulong next = 0;

    double accessed_value;

    for(ulong i = 1; i < 1+1; i++) {{

        accessed_value = in[next + (0 * STEP) * i];
        if (accessed_value != 0)    accessed_value = 0;
        next = (ulong) accessed_value;

        accessed_value = in[next + (1 * STEP) * i];
        if (accessed_value != 0)    accessed_value = 0;
        next = (ulong) accessed_value;
    }}
    out[gid] = accessed_value;
}}")
            .replace(" ", "");

        let generated_kernel = buff_lat.create_kernel().replace(" ", "");
        assert_eq!(generated_kernel, kernel);
    }

    #[test]
    fn print_vector_kernel() {
        let buff_lat = buff_lat::BufferLat::<ocl::aliases::ClFloat16>::new()
            .set_unroll_factor(2_u64)
            .and_then(|m| m.set_step(32 * 1024))
            .and_then(|m| m.set_experiment_name("test"))
            .and_then(|m| m.build())
            .expect("Construction of BufferLat experiment failed");
        let mut kernel = format!("
#define STEP (32768)

 __kernel void testfloat16 (__global float16* in, __global float16* out) {{
    ulong gid = get_global_id(0);
    ulong next = 0;

    float16 accessed_value;

    for(ulong i = 1; i < 1+1; i++) {{

        accessed_value = in[next + (0 * STEP) * i];
        if (accessed_value.s0 != 0)     accessed_value.s0 = 0;
        next = (ulong) accessed_value.s0;

        accessed_value = in[next + (1 * STEP) * i];
        if (accessed_value.s0 != 0)     accessed_value.s0 = 0;
        next = (ulong) accessed_value.s0;
    }}
    out[gid] = accessed_value;
}}");
        let mut generated_kernel = buff_lat.create_kernel();
        // Print stuff here
        generated_kernel = generated_kernel.replace(" ", "");
        kernel = kernel.replace(" ", "");
        assert_eq!(generated_kernel, kernel);
    }
}
