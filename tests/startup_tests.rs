extern crate ocl;
extern crate ubench_rust;

#[cfg(test)]
mod setup_3_items {
    use ocl;
    use ubench_rust::common;
    use ubench_rust::startup_traits;
    use ubench_rust::exp;

    #[test]
    fn test_defaults() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let startup: startup_traits::StartupTime<f64> = startup_traits::StartupTime::new()
            .build()
            .expect("Construction of Startup experiment failed");

        let mut startup_exp = exp::ExpType1::new(startup)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = startup_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }

    #[test]
    fn test_small_setup() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let incr = vec![8];

        let startup: startup_traits::StartupTime<f64> = startup_traits::StartupTime::new()
            .set_min_workitems(1)
            .and_then(|m| m.set_max_workitems(8))
            .and_then(|m| m.set_loops(10))
            .and_then(|m| m.set_incr(incr))
            .and_then(|m| m.build())
            .expect("Construction of Startup experiment failed");

        let mut startup_exp = exp::ExpType1::new(startup)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = startup_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }

    #[test]
    fn test_for_int_type() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let startup: startup_traits::StartupTime<u32> = startup_traits::StartupTime::new()
            .build()
            .expect("Construction of Startup experiment failed");

        let mut startup_exp = exp::ExpType1::new(startup)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = startup_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }

    #[test]
    fn test_for_vector_int_type() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let incr = vec![300];
        let startup: startup_traits::StartupTime<ocl::aliases::ClInt2> =
            startup_traits::StartupTime::new()
                .set_max_workitems(64000)
                .and_then(|m| m.set_incr(incr))
                .and_then(|m| m.build())
                .expect("Construction of Startup experiment failed");

        let mut startup_exp = exp::ExpType1::new(startup)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = startup_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }
}

#[cfg(test)]
mod setup_2_items {
    use ocl;
    use ubench_rust::common;
    use ubench_rust::startup_traits;
    use ubench_rust::traits::GetDimensions;
    use ubench_rust::traits::SetDimensionsParam;
    use ubench_rust::traits::HasKernel;

    #[test]
    fn buffer_size_must_be_equal_to_workitems_for_scalar_types() {
        let common::SetupTest { platform_id, device_id, .. } = common::setup_test();
        let incr = vec![300];
        let mut startup: startup_traits::StartupTime<u32> = startup_traits::StartupTime::new()
            .set_max_workitems(64000)
            .and_then(|m| m.set_incr(incr))
            .and_then(|m| m.build())
            .expect("Construction of Startup experiment failed");

        let plt = ocl::Platform::list()[platform_id];
        let dev = ocl::Device::list_all(&plt).unwrap()[device_id];
        let context = ocl::Context::builder()
            .platform(plt)
            .devices(dev)
            .build()
            .unwrap();
        let queue = ocl::Queue::new(&context, dev).unwrap();
        let program = ocl::Program::builder()
            .src(startup.create_kernel())
            .devices(dev)
            .build(&context)
            .unwrap();

        // Iterate to last, but don't consume the iterator
        let mut param = startup.next();
        while param.is_some() {
            param = startup.next();
            startup.set_global_workdimensions_param(&dev, &queue, &program);
        }

        let gsize = startup.get_global_workdimensions()[0];
        let max_workitems: usize = 64200;
        assert!(gsize == max_workitems,
                "gsize = {} and max_workitems = {}",
                gsize,
                max_workitems);
    }

    #[test]
    fn buffer_size_must_be_equal_workitems_for_vector_types() {
        let common::SetupTest { platform_id, device_id, .. } = common::setup_test();
        let incr = vec![300];
        let mut startup: startup_traits::StartupTime<ocl::aliases::ClInt2> =
            startup_traits::StartupTime::new()
                .set_max_workitems(64000)
                .and_then(|m| m.set_incr(incr))
                .and_then(|m| m.build())
                .expect("Construction of Startup experiment failed");

        let plt = ocl::Platform::list()[platform_id];
        let dev = ocl::Device::list_all(&plt).unwrap()[device_id];
        let context = ocl::Context::builder()
            .platform(plt)
            .devices(dev)
            .build()
            .unwrap();
        let queue = ocl::Queue::new(&context, dev).unwrap();
        let program = ocl::Program::builder()
            .src(startup.create_kernel())
            .devices(dev)
            .build(&context)
            .unwrap();

        // Iterate to last, but don't consume the iterator
        let mut param = startup.next();
        while param.is_some() {
            param = startup.next();
            startup.set_global_workdimensions_param(&dev, &queue, &program);
        }

        let gsize = startup.get_global_workdimensions()[0];
        let max_workitems = 64200 / 2 as usize;
        assert!(gsize == max_workitems,
                "gsize = {} and max_workitems = {}",
                gsize,
                max_workitems);
    }
}

#[cfg(test)]
mod start_time_no_setup {
    use ubench_rust::startup_traits;

    #[test]
    #[should_panic]
    fn start_time_no_setup() {
        startup_traits::StartupTime::<f64>::new()
            .set_experiment_name("89 loads of crap name!!")
            .and_then(|m| m.build())
            .expect("Construction of Startup experiment failed");
    }
}
