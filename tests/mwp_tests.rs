extern crate ocl;
extern crate ubench_rust;

#[cfg(test)]
mod mwp_tests_setup {
    use ubench_rust::mwp_traits;
    use ubench_rust::exp;
    use ubench_rust::common;

    #[test]
    fn test_defaults() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let mwp = mwp_traits::MWP::new();

        let mut mwp_exp = exp::ExpType1::new(mwp)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = mwp_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }

    #[test]
    fn test_mwp_setup() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let mwp = mwp_traits::MWP::new()
            .set_min_workgroups(1)
            .and_then(|m| m.set_max_workgroups(2))
            .and_then(|m| m.set_loops(10))
            .and_then(|m| m.build())
            .expect("Construction of MWP experiment failed");

        let mut mwp_exp = exp::ExpType1::new(mwp)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = mwp_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }

    #[test]
    fn incr_greater_than_workgroups() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let mwp = mwp_traits::MWP::new()
            .set_min_workgroups(1)
            .and_then(|m| m.set_max_workgroups(2))
            .and_then(|m| m.set_incr(10))
            .and_then(|m| m.set_loops(10))
            .and_then(|m| m.build())
            .expect("Construction of MWP experiment failed");

        let mut mwp_exp = exp::ExpType1::new(mwp)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = mwp_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }

    #[test]
    fn different_experiment_name() {
        let common::SetupTest { platform_id, device_id, iter } = common::setup_test();
        let mwp = mwp_traits::MWP::new()
            .set_experiment_name("Something_else")
            .and_then(|m| m.build())
            .expect("construction of MWP experiment failed");

        let mut mwp_exp = exp::ExpType1::new(mwp)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        let result = mwp_exp.execute();
        assert!(result.is_ok(), format!("Error: {:?}", result.unwrap_err()));
    }
}

#[cfg(test)]
mod mwp_tests_no_setup {
    use ubench_rust::mwp_traits;

    #[test]
    #[should_panic]
    fn bad_experiment_name_should_fail() {
        mwp_traits::MWP::new()
            .set_experiment_name("34 bad name ;")
            .and_then(|m| m.build())
            .expect("construction failed");
    }

    #[test]
    #[should_panic]
    fn invalid_workgroup_size_must_fail() {
        mwp_traits::MWP::new()
            .set_min_workgroups(5)
            .and_then(|m| m.set_max_workgroups(2))
            .and_then(|m| m.build())
            .expect("Construction of MWP experiment failed");
    }
}
