use std::f64;

#[derive(Debug, Clone)]
pub struct NStat {
    mean: f64,
    sd: f64,
    done: bool,
    val: Vec<f64>,
}

impl NStat {
    #[allow(dead_code)]
    pub fn new() -> NStat {
        NStat {
            mean: 0f64,
            sd: 0f64,
            done: false,
            val: Vec::new(),
        }
    }

    #[allow(dead_code)]
    pub fn clear(&mut self) {
        self.mean = 0f64;
        self.sd = 0f64;
        self.done = false;
        self.val.clear();
    }

    #[allow(dead_code)]
    pub fn push(&mut self, x: f64) {
        self.val.push(x);
        self.done = false;
    }

    #[allow(dead_code)]
    pub fn mean(&mut self) -> f64 {
        if self.done {
            self.mean
        } else {
            self.calculate();
            self.mean
        }
    }

    #[allow(dead_code)]
    pub fn sd(&mut self) -> f64 {
        if self.done {
            self.sd
        } else {
            self.calculate();
            self.sd
        }
    }

    #[allow(dead_code)]
    pub fn count(&self) -> u64 {
        self.val.len() as u64
    }

    fn calculate(&mut self) {
        if self.val.len() == 0 {
            self.mean = 0f64;
            self.sd = 0f64;
        }

        let mut sum: f64 = 0f64;
        let mut sq_sum: f64 = 0f64;
        let n: f64 = self.val.len() as f64;
        for v in &self.val {
            sum += *v;
            sq_sum += *v * *v;
        }

        self.mean = sum / (self.val.len() as f64);
        self.sd = f64::sqrt((sq_sum / n) - (self.mean * self.mean));
        self.done = true;
    }
}
