use std;

use ocl;

use traits;
use stats;
use ocltypes;
use common;

/// Limit on number of input arguments to a kernel for this experiment
const MAX_KERNEL_IN_ARGS: u32 = 10u32;

/// Struct to hold current value of iteration for the Startup Experiment
#[derive(Debug, Clone)]
pub struct Cur {
    wi: u32, // Current value of workitms
    inc: u32, // Current value of incr
    ind: usize, // Current index of the incr vector
}

#[derive(Debug, Clone)]
pub struct StartupTime<B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType> {
    current_value: Cur,
    min_workitems: u32,
    max_workitems: u32,
    incr: Vec<u32>,
    loops: u32,
    kernel_in_args: u32,
    experiment_name: String,
    global_workitems: ocl::SpatialDims,
    local_workitems: ocl::SpatialDims,
    exec_stats: stats::NStat,
    buffer_type: std::marker::PhantomData<B>,
}

impl<B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType> StartupTime<B> {
    pub fn new() -> StartupTime<B> {
        StartupTime {
            current_value: Cur {
                wi: 0u32,
                inc: 8u32,
                ind: 0usize,
            },
            min_workitems: 1,
            max_workitems: 8,
            incr: vec![8],
            loops: 10,
            kernel_in_args: 2,
            experiment_name: String::from("StartupTime_".to_owned()),
            global_workitems: ocl::SpatialDims::One(1),
            local_workitems: ocl::SpatialDims::One(0),
            exec_stats: stats::NStat::new(),
            buffer_type: std::marker::PhantomData,
        }
    }

    #[allow(dead_code)]
    pub fn set_min_workitems<'a>(&'a mut self,
                                 value: u32)
                                 -> std::result::Result<&'a mut StartupTime<B>, String> {
        if value <= 0 {
            self.min_workitems = 1;
        } else {
            self.min_workitems = value;
        }
        Ok(self)
    }

    #[allow(dead_code)]
    pub fn get_min_workitems(&self) -> u32 {
        self.min_workitems
    }

    #[allow(dead_code)]
    pub fn set_max_workitems<'a>(&'a mut self,
                                 value: u32)
                                 -> std::result::Result<&'a mut StartupTime<B>, String> {
        if value < self.min_workitems {
            Err(format!("Maximum values cannot be smaller than minimum values."))
        } else {
            self.max_workitems = value;
            Ok(self)
        }
    }

    #[allow(dead_code)]
    pub fn get_max_workitems(&self) -> u32 {
        self.max_workitems
    }

    #[allow(dead_code)]
    pub fn set_incr<'a>(&'a mut self,
                        values: Vec<u32>)
                        -> std::result::Result<&'a mut StartupTime<B>, String> {
        for (ii, v) in values.iter().enumerate() {
            if *v > self.max_workitems {
                return Err(format!("Incrementing values cannot be greater than maximum \
                                    workitems. Found {} at {}",
                                   v,
                                   ii));
            }
        }
        self.incr = values;
        Ok(self)
    }

    #[allow(dead_code)]
    pub fn get_incr(&self) -> Vec<u32> {
        self.incr.clone()
    }

    #[allow(dead_code)]
    pub fn set_loops<'a>(&'a mut self,
                         value: u32)
                         -> std::result::Result<&'a mut StartupTime<B>, String> {
        if value <= 0 {
            self.loops = 1;
        } else {
            self.loops = value;
        }
        Ok(self)
    }

    #[allow(dead_code)]
    pub fn get_loops(&self) -> u32 {
        self.loops
    }

    #[allow(dead_code)]
    pub fn set_kernel_in_args<'a>(&'a mut self,
                                  value: u32)
                                  -> std::result::Result<&'a mut StartupTime<B>, String> {
        if value > MAX_KERNEL_IN_ARGS {
            Err(format!("Number of input arguments cannot be greater than max limit: {}",
                        MAX_KERNEL_IN_ARGS))
        } else {
            self.kernel_in_args = value;
            Ok(self)
        }
    }

    #[allow(dead_code)]
    pub fn get_kernel_in_args(&self) -> u32 {
        self.kernel_in_args
    }

    #[allow(dead_code)]
    pub fn set_experiment_name<'a, 'b>(&'a mut self,
                                       value: &'b str)
                                       -> std::result::Result<&'a mut StartupTime<B>, String> {
        if common::is_valid_function_string(value) {
            self.experiment_name = String::from(value);
            Ok(self)
        } else {
            Err("Not a valid name of a function. It must follow convention of a C function".to_owned())
        }
    }

    #[allow(dead_code)]
    pub fn get_experiment_name(&self) -> String {
        let ocl_typ: B = Default::default();
        format!("{}{}", self.experiment_name, ocl_typ.get_opencl_type())
    }

    pub fn build(&self) -> std::result::Result<StartupTime<B>, String> {
        if self.max_workitems < self.min_workitems {
            Err(format!("Maximum values cannot be smaller than minimum values."))
        } else {
            Ok(StartupTime {
                current_value: Cur {
                    wi: 0u32,
                    inc: self.incr[0],
                    ind: 0usize,
                },
                min_workitems: self.min_workitems,
                max_workitems: self.max_workitems,
                incr: self.incr.clone(),
                loops: self.loops,
                kernel_in_args: self.kernel_in_args,
                experiment_name: self.experiment_name.clone(),
                global_workitems: ocl::SpatialDims::One(1),
                local_workitems: ocl::SpatialDims::One(0),
                exec_stats: stats::NStat::new(),
                buffer_type: std::marker::PhantomData,
            })
        }
    }
}

/// The implementation populates parameter in this way
/// The `incr` parameter of struct is a vector with different incrementing values
/// Workitems between `min_workitems` and `max_workitems` is created for each of these incrementing
/// values
impl<B> std::iter::Iterator for StartupTime<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    type Item = Cur;

    fn next(&mut self) -> Option<Cur> {
        let last_elem = self.incr.last();

        // The incr vector must'nt be empty. If its, then there is nothing to iterate on
        if last_elem.is_none() {
            return None;
        }

        let mut wi = self.current_value.wi;
        let mut inc = self.current_value.inc;
        let mut ind = self.current_value.ind;

        if wi >= self.max_workitems {
            // Reached last value
            if ind >= self.incr.len() {
                return None;
            } else {
                // Set next increment value
                inc = self.incr[ind];
                ind = ind + 1usize;
                wi = self.get_min_workitems();
            }
        } else {
            wi = wi + inc;
        }

        self.current_value.wi = wi;
        self.current_value.inc = inc;
        self.current_value.ind = ind;

        Some(self.current_value.clone())
    }
}

impl<B> traits::HasKernel for StartupTime<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    fn create_kernel(&self) -> String {
        let ocl_typ: B = Default::default();

        let mut kernel = format!("
 __kernel void {exp}{typ} (__global {typ}* in0",
                                 exp = self.experiment_name,
                                 typ = ocl_typ.get_opencl_type());
        for i in 1..self.kernel_in_args {
            kernel.push_str(&format!(", __global {typ}* in{ii}",
                                     typ = ocl_typ.get_opencl_type(),
                                     ii = i));
        }
        kernel.push_str(&format!(", __global {typ}* out){{
                uint gid = get_global_id(0);
              }}
            ",
                                 typ = ocl_typ.get_opencl_type()));
        kernel
    }
}

impl<B> traits::SetDimensionsParam for StartupTime<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    fn set_global_workdimensions_param(&mut self,
                                       _: &ocl::Device,
                                       _: &ocl::Queue,
                                       _: &ocl::Program) {
        let ocl_typ: B = Default::default();
        let element_vector_width = ocl_typ.get_width();
        let wi: usize = self.current_value.wi as usize / element_vector_width;
        self.global_workitems = ocl::SpatialDims::One(wi);
    }
}

impl<B> traits::GetDimensions for StartupTime<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    fn get_global_workdimensions(&self) -> ocl::SpatialDims {
        self.global_workitems.clone()
    }
}

impl<B> traits::IntoInputBufferParam for StartupTime<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    type Buff = B;

    fn populate_input_buffer_param(&self) -> Vec<Vec<Self::Buff>> {
        let gsize = self.max_workitems as usize;

        let mut buffs: Vec<Vec<B>> = Vec::new();
        let def: B = Default::default();
        for _ in 0..self.kernel_in_args {
            buffs.push(vec![ def; gsize]);
        }
        buffs
    }
}

impl<B> traits::IntoOutputBufferParam for StartupTime<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    type Buff = B;

    fn populate_output_buffer_param(&self) -> Vec<Vec<Self::Buff>> {
        let gsize = self.max_workitems as usize;

        let def: B = Default::default();
        vec![vec![def; gsize]]
    }
}

impl<B> traits::HasStats for StartupTime<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    fn push_time(&mut self, event: &ocl::Event, mp: traits::MeasuringPoint) {
        match mp {
            traits::MeasuringPoint::Execute => {
                let start_time = match event.profiling_info(ocl::enums::ProfilingInfo::Start) {
                    ocl::enums::ProfilingInfoResult::Start(s) => s,
                    ocl::enums::ProfilingInfoResult::Error(e) => panic!("{:?}", *e),
                    _ => panic!("Match enums properly"),
                };

                let end_time = match event.profiling_info(ocl::enums::ProfilingInfo::End) {
                    ocl::enums::ProfilingInfoResult::End(s) => s,
                    ocl::enums::ProfilingInfoResult::Error(e) => panic!("{:?}", *e),
                    _ => panic!("Match enums properly- end_time"),
                };
                let duration = end_time - start_time;
                self.exec_stats.push(duration as f64);
            }
            _ => {}
        }
    }

    fn clear_stats(&mut self, mp: traits::MeasuringPoint) {
        match mp {
            traits::MeasuringPoint::Execute => {
                self.exec_stats.clear();
            }

            _ => {}
        }
    }
}

impl<B> traits::Exp for StartupTime<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    fn get_header(&self) -> String {
        format!("experiment, type,  step, workitems, opencl_exec_us")
    }

    fn get_stats(&mut self, _: &ocl::Device, _: &ocl::Queue, _: &ocl::Program) -> String {
        let ocl_typ: B = Default::default();
        let mean_in_secs = self.exec_stats.mean() * 10e-9;
        let wi = self.current_value.wi;
        let inc = self.current_value.inc;

        format!("StartupTime, {}, {}, {}, {}\n",
                ocl_typ.get_opencl_type(),
                inc,
                wi,
                mean_in_secs)
    }

    fn get_experiment_name(&self) -> String {
        let ocl_typ: B = Default::default();
        format!("{}{}", self.experiment_name, ocl_typ.get_opencl_type())
    }
}
