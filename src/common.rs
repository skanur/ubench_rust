extern crate regex;
extern crate ocl;

/// When maximum allocatable memory is same as global memory
/// we use this constant to divide the global memory to a smaller value
/// so that other processes can use ram. This is important if using vector
/// types as some platform need more memory to process vector types
/// 16 seems to be a safe value
pub const DIVIDE_GLOBAL_MEMORY: u64 = 16;

pub fn is_valid_function_string(fun_str: &str) -> bool {
    // pattern
    let pattern: &str = r"^[a-zA-Z_][:word:]+$";
    let reg_engine = regex::RegexBuilder::new(pattern)
        .compile()
        .expect("Regex engine couldn't be built");
    reg_engine.is_match(fun_str)
}

#[test]
fn valid_test_names() {
    assert!(is_valid_function_string("_hello"));
    assert!(is_valid_function_string("acde"));
    assert!(is_valid_function_string("A3cd3"));
    assert!(is_valid_function_string("____"));
}

#[test]
fn invalid_test_names() {
    assert!(is_valid_function_string("12") == false);
    assert!(is_valid_function_string("_ad    dcde") == false);
    assert!(is_valid_function_string(" aced2ce_") == false);
    assert!(is_valid_function_string("*&aced") == false);
    assert!(is_valid_function_string("(#cdawe") == false);
    assert!(is_valid_function_string("aee\naed") == false);
    assert!(is_valid_function_string("aee.aed") == false);
}

#[test]
fn long_valid_names() {
    assert!(is_valid_function_string("ceadceadae1233porqwepofiqccdnasfw4t3qedsavdwr2893702casqweorqwepruwqfnsdf23534t3rgfqivwq"));
}

pub fn get_max_allocatable_mem(plt_id: usize, dev_id: usize) -> u64 {
    let plt = ocl::Platform::list()[plt_id];
    let dev = ocl::Device::list_all(&plt).unwrap()[dev_id];
    get_max_allocatable_mem_device(&dev)
}

pub fn get_max_allocatable_mem_device(dev: &ocl::Device) -> u64 {

    let mut max_mem_alloc = match dev.info(ocl::enums::DeviceInfo::MaxMemAllocSize) {
        ocl::enums::DeviceInfoResult::MaxMemAllocSize(mem) => mem,
        _ => {
            panic!("Couldn't get maximum memory allocatable size of device in line: {} and \
                        file: {}",
                   line!(),
                   file!())
        }
    };

    let global_mem = match dev.info(ocl::enums::DeviceInfo::GlobalMemSize) {
        ocl::enums::DeviceInfoResult::GlobalMemSize(gmem) => gmem,
        _ => {
            panic!("Couldnt find global memory size of device at line: {} and \
                                    file: {}",
                   line!(),
                   file!())
        }
    };

    if max_mem_alloc == global_mem {
        max_mem_alloc = global_mem / DIVIDE_GLOBAL_MEMORY;
    }
    max_mem_alloc
}

pub struct SetupTest {
    pub platform_id: usize,
    pub device_id: usize,
    pub iter: u32,
}

pub fn setup_test() -> SetupTest {
    let platform_id = env!("PLATFORM")
        .parse::<usize>()
        .map_err(|e| ocl::Error::String(e.to_string()))
        .expect("Couldn't parse PLATFORM");
    let device_id = env!("DEVICE")
        .parse::<usize>()
        .map_err(|e| ocl::Error::String(e.to_string()))
        .expect("Couldn't parse DEVICE");
    let iter = 1u32;

    SetupTest {
        platform_id: platform_id,
        device_id: device_id,
        iter: iter,
    }
}
