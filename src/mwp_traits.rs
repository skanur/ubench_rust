use std;

use ocl;

use traits;
use stats;
use common;

use ocltypes::HasOpenCLType;
use ocltypes::HasSize;

#[derive(Clone)]
pub struct MWP {
    current_value: u32,
    max_workgroups: u32,
    min_workgroups: u32,
    incr: u32,
    loops: u32,
    experiment_name: String,
    global_workitems: ocl::SpatialDims,
    local_workitems: ocl::SpatialDims,
    exec_stats: stats::NStat,
}

impl MWP {
    pub fn new() -> MWP {
        MWP {
            current_value: 0u32,
            min_workgroups: 1u32,
            max_workgroups: 2u32,
            loops: 10u32,
            incr: 1u32,
            experiment_name: String::from("MWPCoalesced_"),
            global_workitems: ocl::SpatialDims::One(0),
            local_workitems: ocl::SpatialDims::One(0),
            exec_stats: stats::NStat::new(),
        }
    }

    #[allow(dead_code)]
    pub fn set_incr<'a>(&'a mut self,
                        value: u32)
                        -> std::result::Result<&'a mut MWP, &'static str> {
        self.incr = value;
        Ok(self)
    }

    #[allow(dead_code)]
    pub fn get_incr(&self) -> u32 {
        self.incr
    }

    #[allow(dead_code)]
    pub fn set_max_workgroups<'a>(&'a mut self,
                                  value: u32)
                                  -> std::result::Result<&'a mut MWP, &'static str> {
        if value < self.min_workgroups {
            Err("Maximum values cannot be smaller than minimum values.")
        } else {
            self.max_workgroups = value;
            Ok(self)
        }
    }

    #[allow(dead_code)]
    pub fn get_max_workgroups(&self) -> u32 {
        self.max_workgroups
    }

    #[allow(dead_code)]
    pub fn set_min_workgroups<'a>(&'a mut self,
                                  value: u32)
                                  -> std::result::Result<&'a mut MWP, &'static str> {
        if value <= 0 {
            self.min_workgroups = 1;
        } else {
            self.min_workgroups = value;
        }
        Ok(self)
    }

    #[allow(dead_code)]
    pub fn get_min_workgroups(&self) -> u32 {
        self.min_workgroups
    }

    #[allow(dead_code)]
    pub fn set_loops<'a>(&'a mut self,
                         value: u32)
                         -> std::result::Result<&'a mut MWP, &'static str> {
        if value <= 0 {
            self.loops = 1;
        } else {
            self.loops = value;
        }
        Ok(self)
    }

    #[allow(dead_code)]
    pub fn get_loops(&self) -> u32 {
        self.loops
    }

    #[allow(dead_code)]
    pub fn set_experiment_name<'a, 'b>(&'a mut self,
                                       value: &'b str)
                                       -> std::result::Result<&'a mut MWP, &'static str> {
        if common::is_valid_function_string(value) {
            self.experiment_name = String::from(value);
            Ok(self)
        } else {
            Err("Not a valid name of a function. It must follow convention of a C function")
        }
    }

    #[allow(dead_code)]
    pub fn get_experiment_name(&self) -> String {
        self.experiment_name.clone()
    }

    pub fn build(&self) -> std::result::Result<MWP, &'static str> {
        Ok(MWP {
            current_value: 0u32,
            incr: self.incr,
            min_workgroups: self.min_workgroups,
            max_workgroups: self.max_workgroups,
            loops: self.loops,
            experiment_name: self.experiment_name.clone(),
            global_workitems: ocl::SpatialDims::One(0),
            local_workitems: ocl::SpatialDims::One(0),
            exec_stats: stats::NStat::new(),
        })
    }
}

impl std::iter::Iterator for MWP {
    type Item = u32;

    fn next(&mut self) -> Option<u32> {
        self.current_value += self.incr;

        if self.current_value < self.max_workgroups {
            Some(self.current_value)
        } else {
            None
        }
    }
}

impl traits::HasKernel for MWP {
    fn create_kernel(&self) -> String {
        let ocl_typ: f64 = Default::default();
        let mut kernel = format!("
            __kernel void {exp}_{typ} (__global {typ}* in, __global {typ}* out) {{
            uint next = 0;
            uint gid = get_global_id(0);
            uint lid = get_local_id(0);

            next = (uint) in[gid];

            if(next != gid) {{
            printf(\" next(%d) != gid (%d)\\n\", next, gid);
            }}

            ",
                                 exp = self.experiment_name,
                                 typ = ocl_typ.get_opencl_type());

        for _ in 0..self.loops {
            kernel.push_str(&format!("
                next = (uint) in[next];
                next = (uint) in[next];
                next = (uint) in[next];"));
        }
        kernel.push_str(&format!("
            out[gid] = ({typ}) next;
            }}",
                                 typ = ocl_typ.get_opencl_type()));

        kernel
    }
}

impl traits::SetDimensionsParam for MWP {
    fn set_local_workdimensions_param(&mut self,
                                      dev: &ocl::Device,
                                      _: &ocl::Queue,
                                      _: &ocl::Program) {
        let max_workitems = match dev.info(ocl::enums::DeviceInfo::MaxWorkGroupSize) {
            ocl::enums::DeviceInfoResult::MaxWorkGroupSize(size) => size,
            _ => panic!("Cannot get max count of workitems"),
        };
        self.local_workitems = ocl::SpatialDims::One(max_workitems);
    }

    fn set_global_workdimensions_param(&mut self,
                                       dev: &ocl::Device,
                                       _: &ocl::Queue,
                                       _: &ocl::Program) {
        let max_workitems = match dev.info(ocl::enums::DeviceInfo::MaxWorkGroupSize) {
            ocl::enums::DeviceInfoResult::MaxWorkGroupSize(size) => size,
            _ => panic!("Cannot get max count of workitems"),
        };
        let global_workitems = (self.current_value as usize) * max_workitems;
        self.global_workitems = ocl::SpatialDims::One(global_workitems);
    }
}

impl traits::GetDimensions for MWP {
    fn get_global_workdimensions(&self) -> ocl::SpatialDims {
        self.global_workitems.clone()
    }

    fn get_local_workdimensions(&self) -> Option<ocl::SpatialDims> {
        Some(self.local_workitems.clone())
    }
}

use traits::GetDimensions;
impl traits::IntoInputBufferParam for MWP {
    type Buff = f64;

    fn populate_input_buffer_param(&self) -> Vec<Vec<f64>> {
        let lsize = match self.get_local_workdimensions() {
            Some(ls) => ls.to_lens().unwrap(),
            None => panic!("This can't happen!"),
        };
        let in_size = lsize[0] * self.current_value as usize;
        let mut in_data: Vec<f64> = Vec::new();
        for ii in 0..in_size {
            in_data.push(ii as f64);
        }
        vec![in_data]
    }
}

impl traits::IntoOutputBufferParam for MWP {
    type Buff = f64;

    fn populate_output_buffer_param(&self) -> Vec<Vec<f64>> {
        let lsize = match self.get_local_workdimensions() {
            Some(ls) => ls.to_lens().unwrap(),
            None => panic!("This can't happen!"),
        };
        let out_size = lsize[0] * self.current_value as usize;
        let out_data: Vec<f64> = vec![0 as f64; out_size];
        vec![out_data]
    }
}

impl traits::HasStats for MWP {
    fn push_time(&mut self, event: &ocl::Event, mp: traits::MeasuringPoint) {
        match mp {
            traits::MeasuringPoint::Execute => {
                let start_time = match event.profiling_info(ocl::enums::ProfilingInfo::Start) {
                    ocl::enums::ProfilingInfoResult::Start(s) => s,
                    ocl::enums::ProfilingInfoResult::Error(e) => panic!("{:?}", *e),
                    _ => panic!("Match enums properly"),
                };

                let end_time = match event.profiling_info(ocl::enums::ProfilingInfo::End) {
                    ocl::enums::ProfilingInfoResult::End(s) => s,
                    ocl::enums::ProfilingInfoResult::Error(e) => panic!("{:?}", *e),
                    _ => panic!("Match enums properly- end_time"),
                };
                let duration = end_time - start_time;
                self.exec_stats.push(duration as f64);
            }
            _ => {}
        }
    }

    fn clear_stats(&mut self, mp: traits::MeasuringPoint) {
        match mp {
            traits::MeasuringPoint::Execute => {
                self.exec_stats.clear();
            }
            _ => {}
        }
    }
}

impl traits::Exp for MWP {
    fn get_header(&self) -> String {
        format!("type, workgroup count, bytes, exectime, throughput")
    }

    fn get_stats(&mut self, _: &ocl::Device, _: &ocl::Queue, _: &ocl::Program) -> String {
        let ocl_typ: f64 = Default::default();
        let bytes = ocl_typ.get_size() * self.current_value as usize *
                    self.local_workitems[0] as usize * (self.loops as usize);
        let mean_in_secs = self.exec_stats.mean() * 10e-9;
        format!("{}, {}, {}, {}, {}\n",
                ocl_typ.get_opencl_type(),
                self.current_value,
                bytes,
                mean_in_secs,
                bytes as f64 / mean_in_secs)
    }

    fn get_experiment_name(&self) -> String {
        let ocl_typ: f64 = Default::default();
        format!("{}_{}", self.experiment_name, ocl_typ.get_opencl_type())
    }
}
