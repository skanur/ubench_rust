use std;

use ocl;

use traits;
use stats;
use ocltypes;
use common;

/// Limit on buffer size in MB
const MAX_BUFFER_SIZE: u32 = 1024;

/// Struct to hold current value of iteration for the BufferReadWrite Experiment
#[derive(Debug, Clone)]
pub struct CurBuffer {
    inc: u32, // Current value of incr
    ind: usize, // Current index of the incr vector
}

#[derive(Debug, Clone)]
pub struct BufferReadWrite<B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType> {
    current_value: CurBuffer,
    incr: Vec<u32>,
    experiment_name: String,
    local_workitems: ocl::SpatialDims,
    global_workitems: ocl::SpatialDims,
    read_stats: stats::NStat,
    write_stats: stats::NStat,
    buffer_type: std::marker::PhantomData<B>,
}

impl<B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType> BufferReadWrite<B> {
    pub fn new() -> BufferReadWrite<B> {
        BufferReadWrite {
            current_value: CurBuffer {
                inc: 8u32,
                ind: 0usize,
            },
            incr: vec![1],
            experiment_name: String::from("BufferReadWrite_".to_owned()),
            local_workitems: ocl::SpatialDims::One(0),
            global_workitems: ocl::SpatialDims::One(1),
            read_stats: stats::NStat::new(),
            write_stats: stats::NStat::new(),
            buffer_type: std::marker::PhantomData,
        }
    }

    #[allow(dead_code)]
    pub fn set_incr<'a>(&'a mut self,
                        values: Vec<u32>)
                        -> std::result::Result<&'a mut BufferReadWrite<B>, String> {
        for (ii, v) in values.iter().enumerate() {
            if *v > MAX_BUFFER_SIZE {
                return Err(format!("Incrementing values cannot be greater than maximum \
                                    buffersize. Found {} at {}",
                                   v,
                                   ii));
            }
        }
        self.incr = values;
        Ok(self)
    }

    #[allow(dead_code)]
    pub fn get_incr(&self) -> Vec<u32> {
        self.incr.clone()
    }

    #[allow(dead_code)]
    pub fn set_experiment_name<'a, 'b>
        (&'a mut self,
         value: &'b str)
         -> std::result::Result<&'a mut BufferReadWrite<B>, String> {
             if common::is_valid_function_string(value) {
                 self.experiment_name = String::from(value);
                 Ok(self)
             } else {
                 Err("Not a valid name of a function. It must follow convention of a C function".to_owned())
             }
    }

    #[allow(dead_code)]
    pub fn get_experiment_name(&self) -> String {
        let ocl_typ: B = Default::default();
        format!("{}{}", self.experiment_name, ocl_typ.get_opencl_type())
    }

    pub fn build(&self) -> std::result::Result<BufferReadWrite<B>, String> {
        Ok(BufferReadWrite {
            current_value: CurBuffer {
                inc: self.incr[0],
                ind: 0usize,
            },
            incr: self.incr.clone(),
            experiment_name: self.experiment_name.clone(),
            local_workitems: ocl::SpatialDims::One(0),
            global_workitems: ocl::SpatialDims::One(1),
            read_stats: stats::NStat::new(),
            write_stats: stats::NStat::new(),
            buffer_type: std::marker::PhantomData,
        })
    }
}

/// The implementation populates parameter in this way
/// The `incr` parameter of struct is a vector with different incrementing values
/// Buffersize between 1 and MAX_BUFFER_SIZE is created for each of these incrementing
/// values
impl<B> std::iter::Iterator for BufferReadWrite<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    type Item = CurBuffer;

    fn next(&mut self) -> Option<CurBuffer> {
        let last_elem = self.incr.last();

        // The incr vector must'nt be empty. If its, then there is nothing to iterate on
        if last_elem.is_none() {
            return None;
        }

        let inc: u32;
        let mut ind = self.current_value.ind;

        // Reached last value in the vector
        if ind >= self.incr.len() {
            return None;
        } else {
            // Set next increment value
            inc = self.incr[ind];
            ind = ind + 1usize;
        }

        self.current_value.inc = inc;
        self.current_value.ind = ind;

        Some(self.current_value.clone())
    }
}

impl<B> traits::HasKernel for BufferReadWrite<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    fn create_kernel(&self) -> String {
        let ocl_typ: B = Default::default();
        format!("
 __kernel void {exp}{typ} (__global {typ}* in, __global {typ}* out) {{
     uint gid = get_global_id(0);
     out[gid] = in[gid];
 }}",
                exp = self.experiment_name,
                typ = ocl_typ.get_opencl_type())
    }
}

impl<B> traits::SetDimensionsParam for BufferReadWrite<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    fn set_global_workdimensions_param(&mut self,
                                       _: &ocl::Device,
                                       _: &ocl::Queue,
                                       _: &ocl::Program) {
        let ocl_typ: B = Default::default();
        let element_vector_width = ocl_typ.get_width();
        let buffersize = self.current_value.inc as usize;
        let actual_workitems = (buffersize * 1024 * 1024) / element_vector_width;
        self.global_workitems = ocl::SpatialDims::One(actual_workitems);
    }
}

impl<B> traits::GetDimensions for BufferReadWrite<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    fn get_global_workdimensions(&self) -> ocl::SpatialDims {
        self.global_workitems.clone()
    }
}

use traits::GetDimensions;
impl<B> traits::IntoInputBufferParam for BufferReadWrite<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    type Buff = B;

    fn populate_input_buffer_param(&self) -> Vec<Vec<Self::Buff>> {
        let gsize = self.get_global_workdimensions()[0];

        let def: B = Default::default();
        vec![vec![def; gsize]]
    }

    fn get_input_flags_param(&self) -> ocl::flags::MemFlags {
        ocl::core::MEM_READ_WRITE
    }
}

impl<B> traits::IntoOutputBufferParam for BufferReadWrite<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    type Buff = B;

    fn populate_output_buffer_param(&self) -> Vec<Vec<Self::Buff>> {
        let gsize = self.get_global_workdimensions().to_lens().unwrap()[0];

        let def: B = Default::default();
        vec![vec![def; gsize]]
    }
}

impl<B> traits::HasStats for BufferReadWrite<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    fn push_time(&mut self, event: &ocl::Event, mp: traits::MeasuringPoint) {
        match mp {
            traits::MeasuringPoint::ReadBuffer => {
                let start_time = match event.profiling_info(ocl::enums::ProfilingInfo::Start) {
                    ocl::enums::ProfilingInfoResult::Start(s) => s,
                    ocl::enums::ProfilingInfoResult::Error(e) => panic!("{:?}", *e),
                    _ => panic!("Match enums properly"),
                };

                let end_time = match event.profiling_info(ocl::enums::ProfilingInfo::End) {
                    ocl::enums::ProfilingInfoResult::End(s) => s,
                    ocl::enums::ProfilingInfoResult::Error(e) => panic!("{:?}", *e),
                    _ => panic!("Match enums properly- end_time"),
                };
                let duration = end_time - start_time;
                self.read_stats.push(duration as f64);
            }

            traits::MeasuringPoint::WriteBuffer => {
                let start_time = match event.profiling_info(ocl::enums::ProfilingInfo::Start) {
                    ocl::enums::ProfilingInfoResult::Start(s) => s,
                    ocl::enums::ProfilingInfoResult::Error(e) => panic!("{:?}", *e),
                    _ => panic!("Match enums properly"),
                };

                let end_time = match event.profiling_info(ocl::enums::ProfilingInfo::End) {
                    ocl::enums::ProfilingInfoResult::End(s) => s,
                    ocl::enums::ProfilingInfoResult::Error(e) => panic!("{:?}", *e),
                    _ => panic!("Match enums properly- end_time"),
                };
                let duration = end_time - start_time;
                self.write_stats.push(duration as f64);
            }
            _ => {}
        }
    }

    fn clear_stats(&mut self, mp: traits::MeasuringPoint) {
        match mp {
            traits::MeasuringPoint::ReadBuffer => {
                self.read_stats.clear();
            }

            traits::MeasuringPoint::WriteBuffer => {
                self.write_stats.clear();
            }

            _ => {}
        }
    }
}

impl<B> traits::Exp for BufferReadWrite<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    fn measure_write_times(&self) -> bool {
        true
    }

    fn measure_read_times(&self) -> bool {
        true
    }

    fn get_header(&self) -> String {
        format!("experiment, type,  buffer_size, buffer_write_time, buffer_read_time")
    }

    fn get_stats(&mut self, _: &ocl::Device, _: &ocl::Queue, _: &ocl::Program) -> String {
        let ocl_typ: B = Default::default();
        let read_mean_in_secs = self.read_stats.mean() * 10e-9;
        let write_mean_in_secs = self.write_stats.mean() * 10e-9;
        let gsize = self.get_global_workdimensions()[0];

        format!("BufferReadWrite, {}, {}, {}, {}\n",
                ocl_typ.get_opencl_type(),
                gsize,
                write_mean_in_secs,
                read_mean_in_secs)
    }

    fn get_experiment_name(&self) -> String {
        let ocl_typ: B = Default::default();
        format!("{}{}", self.experiment_name, ocl_typ.get_opencl_type())
    }
}
