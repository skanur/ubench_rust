use std;
use std::io::Write;

use ocl;
use ocl_core;
use time;

use traits;

// Experiment type 1
// That has parameterized input and output, but the kernel is not dependent on the parameter
//
#[allow(dead_code)]
pub struct ExpType1<T>
where T : std::iter::Iterator + traits::HasKernel + traits::IntoInputBufferParam + traits::IntoOutputBufferParam + traits::SetDimensionsParam  + traits::GetDimensions + traits::Exp + traits::HasStats + Clone
{
    exp: T,
    platform_id: usize,
    device_id: usize,
    max_iterations: u32,
}

#[allow(dead_code)]
impl<T> ExpType1<T>
where T : std::iter::Iterator + traits::HasKernel + traits::IntoInputBufferParam + traits::IntoOutputBufferParam + traits::SetDimensionsParam  + traits::GetDimensions + traits::Exp + traits::HasStats + Clone
{
    #[allow(dead_code)]
    pub fn new(exp: T) -> ExpType1<T> {
        ExpType1 {
            exp: exp,
            platform_id: 0usize,
            device_id: 0usize,
            max_iterations: 1u32,
        }
    }

    #[allow(dead_code)]
    pub fn set_platform_id<'a> (&'a mut self, value: usize) -> &'a mut ExpType1<T> {
        self.platform_id = value;
        self
    }

    #[allow(dead_code)]
    pub fn set_device_id<'a> (&'a mut self, value: usize) -> &'a mut ExpType1<T> {
        self.device_id = value;
        self
    }

    #[allow(dead_code)]
    pub fn set_max_iterations<'a> (&'a mut self, value: u32) -> &'a mut ExpType1<T> {
        self.max_iterations = value;
        self
    }

    #[allow(dead_code)]
    pub fn get_platform_id(&self) -> usize {
        self.platform_id
    }

    #[allow(dead_code)]
    pub fn get_device_id(&self) -> usize {
        self.device_id
    }

    #[allow(dead_code)]
    pub fn get_max_iterations(&self) -> u32 {
        self.max_iterations
    }

    #[allow(dead_code)]
    pub fn build(&self) -> ExpType1<T> {
        ExpType1 {
            exp : self.exp.clone(),
            platform_id : self.platform_id,
            device_id: self.device_id,
            max_iterations: self.max_iterations,
        }
    }

    fn measure_buffer_write_times<D>(&mut self, que: &ocl::Queue, buf: &ocl::Buffer<D>, datum: &[D])
        where D: ocl::traits::OclPrm
    {
        let iter = self.max_iterations;
        for _ in 0..iter {
            let mut event = ocl::EventList::new();
            buf.cmd().write(datum).enew(&mut event).enq().unwrap();
            que.finish();
            let last_event = event.last_clone().unwrap();
            self.exp.push_time(&last_event, traits::MeasuringPoint::WriteBuffer);
        }
    }

    fn measure_buffer_read_times<D>(&mut self, que: &ocl::Queue, buf: &ocl::Buffer<D>)
        where D: ocl::traits::OclPrm
    {
        let iter = self.max_iterations;
        for _ in 0..iter {
            let def : D = Default::default();
            let mut datum : Vec<D> = vec![def; buf.len()];
            let mut event = ocl::EventList::new();
            buf.cmd().read(&mut datum).enew(&mut event).enq().unwrap();
            que.finish();
            let last_event = event.last_clone().unwrap();
            self.exp.push_time(&last_event, traits::MeasuringPoint::ReadBuffer);
        }
    }


    #[allow(dead_code)]
    pub fn execute(&mut self) -> Result<(), ocl::Error> {

        let plt = ocl::Platform::list()[self.platform_id];
        let dev = try!(ocl::Device::list_all(&plt))[self.device_id];
        let mut stderr = std::io::stderr();
        writeln!(&mut stderr, "Platform: {}\nDevice: {}\n", plt.name(), dev.name()).unwrap();
        let context = try!(ocl::Context::builder()
            .platform(plt)
            .devices(dev)
            .build());
        let queue = try!(ocl::Queue::new(&context, dev));
        let program = try!(ocl::Program::builder()
            .src(self.exp.create_kernel())
            .devices(dev)
            .cmplr_opt("-cl-opt-disable")
            .build(&context));

        // Reset the measuring points
        self.exp.clear_stats(traits::MeasuringPoint::Execute);
        self.exp.clear_stats(traits::MeasuringPoint::ReadBuffer);
        self.exp.clear_stats(traits::MeasuringPoint::WriteBuffer);
        self.exp.clear_stats(traits::MeasuringPoint::Others(0));

        let mut param = self.exp.next();
        println!("{}", self.exp.get_header());
        while param.is_some() {
            // Start wall clock for this iteration
            self.exp.push_non_event_time(time::get_time(), traits::MeasuringPoint::Others(0));

            self.exp.set_local_workdimensions_param(&dev, &queue, &program);
            self.exp.set_global_workdimensions_param(&dev, &queue, &program);

            let in_mem_flags = self.exp.get_input_flags_param();
            let in_datas = self.exp.populate_input_buffer_param();
            let mut in_buffs : Vec<ocl::Buffer< <T as traits::IntoInputBufferParam>::Buff >> = Vec::new();
            for datum in &in_datas {
                let in_dims = [datum.len()];
                let mut in_buff_data : Option<&[<T as traits::IntoInputBufferParam>::Buff]> = None;
                if in_mem_flags.contains(ocl::core::MEM_USE_HOST_PTR) || in_mem_flags.contains(ocl::core::MEM_COPY_HOST_PTR) {
                    in_buff_data = Some(&datum);
                }
                let in_buff = try!(ocl::Buffer::new(queue.clone(), Some(in_mem_flags), &in_dims, in_buff_data));

                // Perform mapping of buffer and copying of the data
                if in_mem_flags.contains(ocl::core::MEM_ALLOC_HOST_PTR) {
                    unsafe {
                        for _ in 0..self.max_iterations {
                            self.exp.push_non_event_time(time::get_time(), traits::MeasuringPoint::Others(2));
                            let mut event = ocl::EventList::new();
                            let buff_datum = try!(ocl_core::enqueue_map_buffer::< <T as traits::IntoInputBufferParam>::Buff >(&queue, in_buff.core_as_ref(), true, ocl_core::MAP_WRITE, 0, datum.len(), None, Some(&mut event))) ;
                            queue.finish();
                            let last_event = event.last_clone().expect(&format!("{}", line!()));
                            self.exp.push_time(&last_event, traits::MeasuringPoint::WriteBuffer);
                            try!(ocl_core::enqueue_unmap_mem_object(&queue, in_buff.core_as_ref(), buff_datum, None, None));
                            queue.finish();
                            self.exp.push_non_event_time(time::get_time(), traits::MeasuringPoint::Others(3));
                        }

                        let buff_datum = try!(ocl_core::enqueue_map_buffer::< <T as traits::IntoInputBufferParam>::Buff >(&queue, in_buff.core_as_ref(), true, ocl_core::MAP_WRITE, 0, datum.len(), None, None)) ;
                        queue.finish();

                        let mut buff_vector = Vec::from_raw_parts(buff_datum as *mut <T as traits::IntoInputBufferParam>::Buff, datum.len(), datum.len());
                        buff_vector.copy_from_slice(&datum);
                        try!(ocl_core::enqueue_unmap_mem_object(&queue, in_buff.core_as_ref(), buff_datum, None, None));
                        queue.finish();
                        std::mem::forget(buff_vector);
                    }
                } else {
                    if self.exp.measure_write_times() {
                        self.measure_buffer_write_times(&queue, &in_buff, datum);
                    }
                }
                in_buffs.push(in_buff);
            }

            let out_mem_flags = self.exp.get_output_flags_param();
            let out_datas = self.exp.populate_output_buffer_param();
            let mut out_buffs : Vec<ocl::Buffer< <T as traits::IntoOutputBufferParam>::Buff >> = Vec::new();
            for datum in &out_datas {
                let out_dims = [datum.len()];
                let mut out_buff_data : Option<&[<T as traits::IntoOutputBufferParam>::Buff]> = None;
                if out_mem_flags.contains(ocl::core::MEM_USE_HOST_PTR) || out_mem_flags.contains(ocl::core::MEM_COPY_HOST_PTR) {
                    out_buff_data = Some(&datum);
                }
                let out_buff = try!(ocl::Buffer::new(queue.clone(), Some(out_mem_flags), &out_dims, out_buff_data));
                out_buffs.push(out_buff);
            }

            let kernel_name = self.exp.get_experiment_name();
            let mut kernel = try!(ocl::Kernel::new(kernel_name.clone(), &program, &queue));
            let gws = self.exp.get_global_workdimensions();
            kernel = kernel.gws(gws);
            let kernel_lws = self.exp.get_local_workdimensions();
            if kernel_lws.is_some() {
                kernel = kernel.lws(kernel_lws.unwrap());
            }
            for buffs in &in_buffs {
                kernel = kernel.arg_buf(buffs);
            }
            for buffs in &out_buffs {
                kernel = kernel.arg_buf(buffs);
            }

            if self.exp.has_scalar_param() {
                let scalar_params = self.exp.populate_scalar_param();
                for scalar in scalar_params {
                    kernel = kernel.arg_scl(scalar);
                }
            }

            try!(kernel.cmd().enq());
            let gws_in_spatial_dim : ocl::SpatialDims = gws;
            let global_workitems = match ocl::traits::WorkDims::to_work_size(&gws as &ocl::SpatialDims)  {
                Some(g) => g,
                None => return ocl::Error::err("Error while getting global dimensions"),
            };
            let dim_count = gws_in_spatial_dim.dim_count();
            let mut local_workitems : Option<[usize; 3]> = None;
            if kernel_lws.is_some() {
                local_workitems = Some(kernel_lws.unwrap().to_lens().unwrap());
            }

            let iter = self.max_iterations;
            for _ in 0..iter {
                let mut event = ocl::EventList::new();

                try!(ocl_core::enqueue_kernel(queue.core_as_ref(), kernel.core_as_ref(), dim_count, None, &global_workitems, local_workitems, None, Some(&mut event)));
                queue.finish();

                let last_event = try!(event.last_clone().ok_or(ocl::Error::String("last_event failed".to_owned())));
                self.exp.push_time(&last_event, traits::MeasuringPoint::Execute);

            }

            // Read the output data and verify
            let out_mem_flags = self.exp.get_output_flags_param();
            let mut verified_data : Vec<Vec<<T as traits::IntoOutputBufferParam>::Buff>> = Vec::new();
            for out_buff in &out_buffs {
                let buffer_size = out_buff.len();
                let mut datum : Vec<<T as traits::IntoOutputBufferParam>::Buff> = vec![Default::default(); buffer_size];
                if out_mem_flags.contains(ocl::core::MEM_ALLOC_HOST_PTR) {
                    unsafe {
                        let buff_datum = try!(ocl_core::enqueue_map_buffer::< <T as traits::IntoOutputBufferParam>::Buff>( &queue, out_buff.core_as_ref(), true, ocl_core::MAP_READ, 0, buffer_size, None, None));
                        queue.finish();
                        let buff_vector = Vec::from_raw_parts(buff_datum as *mut <T as traits::IntoOutputBufferParam>::Buff, buffer_size, buffer_size);
                        datum.copy_from_slice(&buff_vector);
                        try!(ocl_core::enqueue_unmap_mem_object(&queue, out_buff.core_as_ref(), buff_datum, None, None));
                        queue.finish();
                        std::mem::forget(buff_vector);
                    }
                }
                else {
                    out_buff.cmd().read(&mut datum).enq().unwrap();
                    queue.finish();
                }
                verified_data.push(datum);
            }
            try!(self.exp.verify_param(verified_data));

            // Measure the read times
            if self.exp.measure_read_times() {
            let out_mem_flags = self.exp.get_output_flags_param();
                for out_buff in &out_buffs {
                // Perform mapping of buffer and copying of the data
                if out_mem_flags.contains(ocl::core::MEM_ALLOC_HOST_PTR) {
                    let mut datum : Vec<<T as traits::IntoOutputBufferParam>::Buff> = vec![Default::default(); out_buff.len()];
                    unsafe {
                        for _ in 0..self.max_iterations {
                            self.exp.push_non_event_time(time::get_time(), traits::MeasuringPoint::Others(4));
                            let mut event = ocl::EventList::new();
                            let buff_datum = try!(ocl_core::enqueue_map_buffer::< <T as traits::IntoOutputBufferParam>::Buff >(&queue, out_buff.core_as_ref(), true, ocl_core::MAP_READ, 0, datum.len(), None, Some(&mut event))) ;
                            queue.finish();
                            let last_event = event.last_clone().expect(&format!("{}", line!()));
                            self.exp.push_time(&last_event, traits::MeasuringPoint::ReadBuffer);
                            try!(ocl_core::enqueue_unmap_mem_object(&queue, out_buff.core_as_ref(), buff_datum, None, None));
                            queue.finish();
                            self.exp.push_non_event_time(time::get_time(), traits::MeasuringPoint::Others(5));
                        }

                        let buff_datum = try!(ocl_core::enqueue_map_buffer::< <T as traits::IntoOutputBufferParam>::Buff >(&queue, out_buff.core_as_ref(), true, ocl_core::MAP_READ, 0, datum.len(), None, None)) ;
                        queue.finish();

                        let buff_vector = Vec::from_raw_parts(buff_datum as *mut <T as traits::IntoOutputBufferParam>::Buff, datum.len(), datum.len());
                        datum.copy_from_slice(&buff_vector);
                        try!(ocl_core::enqueue_unmap_mem_object(&queue, out_buff.core_as_ref(), buff_datum, None, None));
                        queue.finish();
                        std::mem::forget(buff_vector);
                    }
                } else {
                    self.measure_buffer_read_times(&queue, out_buff);
                }
                }
            }
            // Stop wall clock for this iteration
            self.exp.push_non_event_time(time::get_time(), traits::MeasuringPoint::Others(1));

            println!("{}", self.exp.get_stats(&dev, &queue, &program));

            self.exp.clear_stats(traits::MeasuringPoint::Execute);
            self.exp.clear_stats(traits::MeasuringPoint::ReadBuffer);
            self.exp.clear_stats(traits::MeasuringPoint::WriteBuffer);
            self.exp.clear_stats(traits::MeasuringPoint::Others(0));
            param = self.exp.next();
        }

        Ok(())
    }
}
