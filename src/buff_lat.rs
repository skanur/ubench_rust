use std;

use ocl;
use time;

use traits;
use stats;
use ocltypes;
use common;

/// Minimum ratio of global memory that can be set
/// i.e. if max memory is 'M', then this code sets M/MIN_MEM_RATIO
/// You need atleast 4, so that one can set input and output buffers
/// of host and device
/// 16 seems to be a safe value for an 8 Gb RAM
pub const MIN_MEM_RATIO: u64 = 16;

/// Maximum unrolled loops a kernel can have.
/// Odroid has a limitation of 2500, so setting this globally
pub const MAX_UNROLL_FACTOR: u64 = 2500;

#[derive(Debug, Clone)]
pub struct ReadWriteFlags {
    pub read_flag: ocl::flags::MemFlags,
    pub write_flag: ocl::flags::MemFlags,
}

#[derive(Debug, Clone)]
pub struct BufferLat<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    current_ind: usize,
    flags: Vec<ReadWriteFlags>,
    unroll_factor: u64,
    loops: u64,
    step: u64,
    max_mem_ratio: u64,
    experiment_name: String,
    local_workitems: ocl::SpatialDims,
    global_workitems: ocl::SpatialDims,
    read_stats: stats::NStat,
    write_stats: stats::NStat,
    exec_stats: stats::NStat,
    entire_stats: stats::NStat,
    start_time: time::Timespec,
    end_time: time::Timespec,
    b_read_time: time::Timespec,
    b_write_time: time::Timespec,
    b_read_stats: stats::NStat,
    b_write_stats: stats::NStat,
    buffer_type: std::marker::PhantomData<B>,
}

impl<B> BufferLat<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    pub fn new() -> BufferLat<B> {
        BufferLat {
            current_ind: 0usize,
            flags: vec![ReadWriteFlags {
                            read_flag: ocl::flags::MEM_READ_WRITE,
                            write_flag: ocl::flags::MEM_READ_WRITE,
                        }],
            experiment_name: String::from("BufferLat_".to_owned()),
            unroll_factor: 1_u64,
            loops: 1_u64,
            step: 1_u64,
            max_mem_ratio: MIN_MEM_RATIO,
            local_workitems: ocl::SpatialDims::One(0),
            global_workitems: ocl::SpatialDims::One(1),
            read_stats: stats::NStat::new(),
            write_stats: stats::NStat::new(),
            exec_stats: stats::NStat::new(),
            entire_stats: stats::NStat::new(),
            start_time: time::Timespec::new(0, 0),
            end_time: time::Timespec::new(0, 0),
            b_read_time: time::Timespec::new(0, 0),
            b_write_time: time::Timespec::new(0, 0),
            b_read_stats: stats::NStat::new(),
            b_write_stats: stats::NStat::new(),
            buffer_type: std::marker::PhantomData,
        }
    }

    pub fn get_max_mem_alloc(&self, plt_id: usize, dev_id: usize) -> u64 {
        // Maximum allocatable memory for this experiment
        common::get_max_allocatable_mem(plt_id, dev_id) / self.max_mem_ratio
    }

    /// Set maximum loops for a given unroll factor and step size
    /// Also supply platform and device ID as it needs device specific maximum allocatable global memory
    #[allow(dead_code)]
    pub fn set_max_loops<'a>(&'a mut self,
                             unroll_factor: u64,
                             step: u64,
                             plt_id: usize,
                             dev_id: usize)
                             -> std::result::Result<&'a mut BufferLat<B>, String> {
        if (unroll_factor < 1) || (step < 1) {
            Err("Unroll factor or step cannot be less than 1".to_owned())
        } else {
            let ocl_typ: B = Default::default();
            let element_vector_width = ocl_typ.get_width() as u64;
            // Maximum allocatable memory for this experiment
            let max_mem_alloc = common::get_max_allocatable_mem(plt_id, dev_id) /
                                self.max_mem_ratio;

            // max_mem_alloc = step * loops * unroll_factor * element_vector_width
            // Other way of saying is that, if self = 1, loops = 1, unroll_factor=2 &
            // element_vector_width = 16, then mem_alloc should be = 32
            let max_loops = max_mem_alloc / (step * unroll_factor * element_vector_width);

            // Check the corner cases
            if max_loops < 1 {
                return Err("Reduce either unroll_factor or step".to_owned());
            }
            self.set_step(step)
                .and_then(|m| m.set_unroll_factor(unroll_factor))
                .and_then(|m| m.set_loops(max_loops - 1))
                .expect("set_max_unroll_factor failed");
            Ok(self)
        }
    }

    /// Set maximum unroll factor for a given loop count and step size
    /// Also supply platform and device ID as it needs device specific maximum allocatable global memory
    #[allow(dead_code)]
    pub fn set_max_unroll_factor<'a>(&'a mut self,
                                     loops: u64,
                                     step: u64,
                                     plt_id: usize,
                                     dev_id: usize)
                                     -> std::result::Result<&'a mut BufferLat<B>, String> {
        if (loops < 1) || (step < 1) {
            Err("Unroll factor or step cannot be less than 1".to_owned())
        } else {
            let ocl_typ: B = Default::default();
            let element_vector_width = ocl_typ.get_width() as u64;

            // Maximum allocatable memory for this experiment
            let max_mem_alloc = common::get_max_allocatable_mem(plt_id, dev_id) /
                                self.max_mem_ratio;

            // max_mem_alloc = step * loops * unroll_factor * element_vector_width
            // Other way of saying is that, if self = 1, loops = 1, unroll_factor=2 &
            // element_vector_width = 16, then mem_alloc should be = 32
            let max_unroll_factor = max_mem_alloc / (step * loops * element_vector_width);

            // See the corner cases
            if max_unroll_factor > MAX_UNROLL_FACTOR {
                Err(format!("Unroll factor: {} went beyond MAX_UNROLL_FACTOR of {}.\nReduce \
                             loops count or increase step size\n",
                            max_unroll_factor,
                            MAX_UNROLL_FACTOR))
            } else {
                if max_unroll_factor < 1 {
                    return Err("Reduce either loop count or step".to_owned());
                }
                self.set_step(step)
                    .and_then(|m| m.set_unroll_factor(max_unroll_factor - 1))
                    .and_then(|m| m.set_loops(loops))
                    .expect("set_max_unroll_factor failed");
                Ok(self)
            }
        }
    }

    #[allow(dead_code)]
    pub fn set_unroll_factor<'a>(&'a mut self,
                                 values: u64)
                                 -> std::result::Result<&'a mut BufferLat<B>, String> {
        if values < 1 {
            Err("unroll_factor should be atleast 1".to_owned())
        } else if values > MAX_UNROLL_FACTOR {
            Err(format!("Unroll factor went beyond MAX_UNROLL_FACTOR of {}.\nReduce the value\n",
                        MAX_UNROLL_FACTOR))
        } else {
            self.unroll_factor = values;
            Ok(self)
        }
    }

    #[allow(dead_code)]
    pub fn get_unroll_factor(&self) -> u64 {
        self.unroll_factor
    }

    #[allow(dead_code)]
    pub fn set_loops<'a>(&'a mut self,
                         value: u64)
                         -> std::result::Result<&'a mut BufferLat<B>, String> {
        if value < 1 {
            Err("Step value must be atleast 1".to_owned())
        } else {
            self.loops = value;
            Ok(self)
        }
    }

    #[allow(dead_code)]
    pub fn get_loops(&self) -> u64 {
        self.loops
    }

    #[allow(dead_code)]
    pub fn set_step<'a>(&'a mut self,
                        value: u64)
                        -> std::result::Result<&'a mut BufferLat<B>, String> {
        if value < 1 {
            Err("Step value must be atleast 1".to_owned())
        } else {
            self.step = value;
            Ok(self)
        }
    }

    #[allow(dead_code)]
    pub fn get_step(&self) -> u64 {
        self.step
    }

    #[allow(dead_code)]
    pub fn set_max_mem_ratio<'a>(&'a mut self,
                                 value: u64)
                                 -> std::result::Result<&'a mut BufferLat<B>, String> {
        if value < MIN_MEM_RATIO {
            Err("Step value must be atleast equal to MIN_MEM_RATIO".to_owned())
        } else {
            self.max_mem_ratio = value;
            Ok(self)
        }
    }

    #[allow(dead_code)]
    pub fn get_max_mem_ratio(&self) -> u64 {
        self.max_mem_ratio
    }

    #[allow(dead_code)]
    pub fn set_flags<'a>(&'a mut self,
                         values: &[ReadWriteFlags])
                         -> std::result::Result<&'a mut BufferLat<B>, String> {
        self.flags = values.to_vec();
        Ok(self)
    }

    #[allow(dead_code)]
    pub fn get_flags(&self) -> Vec<ReadWriteFlags> {
        self.flags.clone()
    }

    #[allow(dead_code)]
    pub fn set_experiment_name<'a, 'b>(&'a mut self,
                                       value: &'b str)
                                       -> std::result::Result<&'a mut BufferLat<B>, String> {
        if common::is_valid_function_string(value) {
            self.experiment_name = String::from(value);
            Ok(self)
        } else {
            Err("Not a valid name of a function. It must follow convention of a C function"
                .to_owned())
        }
    }

    #[allow(dead_code)]
    pub fn get_experiment_name(&self) -> String {
        let ocl_typ: B = Default::default();
        format!("{}{}", self.experiment_name, ocl_typ.get_opencl_type())
    }

    pub fn build(&self) -> std::result::Result<BufferLat<B>, String> {
        Ok(BufferLat {
            current_ind: 0usize,
            flags: self.flags.clone(),
            unroll_factor: self.unroll_factor,
            loops: self.loops,
            step: self.step,
            max_mem_ratio: self.max_mem_ratio,
            experiment_name: self.experiment_name.clone(),
            local_workitems: ocl::SpatialDims::One(0),
            global_workitems: ocl::SpatialDims::One(1),
            read_stats: stats::NStat::new(),
            write_stats: stats::NStat::new(),
            exec_stats: stats::NStat::new(),
            entire_stats: stats::NStat::new(),
            start_time: time::Timespec::new(0, 0),
            end_time: time::Timespec::new(0, 0),
            b_read_time: time::Timespec::new(0, 0),
            b_write_time: time::Timespec::new(0, 0),
            b_read_stats: stats::NStat::new(),
            b_write_stats: stats::NStat::new(),
            buffer_type: std::marker::PhantomData,
        })
    }
}

impl<B> std::iter::Iterator for BufferLat<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    type Item = usize;

    fn next(&mut self) -> Option<usize> {
        // The incr vector must'nt be empty. If its, then there is nothing to iterate on
        if self.flags.is_empty() {
            return None;
        }

        let mut ind = self.current_ind;

        // Reached last value in the vector
        if ind >= self.flags.len() {
            return None;
        } else {
            // Set next increment value
            ind = ind + 1usize;
        }

        self.current_ind = ind;

        Some(self.current_ind.clone())
    }
}

impl<B> traits::HasKernel for BufferLat<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    fn create_kernel(&self) -> String {
        let ocl_typ: B = Default::default();
        let element_vector_width = ocl_typ.get_width();

        let mut kernel = format!("
#define STEP ({step})

 __kernel void {exp}{typ} (__global {typ}* in, __global {typ}* out) {{
    ulong gid = get_global_id(0);
    ulong next = 0;

    {typ} accessed_value;
    
    for(ulong i = 1; i < {loops}+1; i++) {{",
                                 step = self.step,
                                 exp = self.experiment_name,
                                 loops = self.loops,
                                 typ = ocl_typ.get_opencl_type());

        if element_vector_width > 1 {
            for ii in 0..self.unroll_factor {
                kernel.push_str(&format!("

        accessed_value = in[next + ({index} * STEP) * i];
        if (accessed_value.s0 != 0)     accessed_value.s0 = 0;
        next = (ulong) accessed_value.s0;",
                                         index = ii));
            }

            kernel.push_str(&format!("
    }}
    out[gid] = accessed_value;
}}"));
        } else {
            // For scalar kernels
            for ii in 0..self.unroll_factor {
                kernel.push_str(&format!("

        accessed_value = in[next + ({index} * STEP) * i];
        if (accessed_value != 0)    accessed_value = 0;
        next = (ulong) accessed_value;",
                                         index = ii));
            }
            kernel.push_str(&format!("
    }}
    out[gid] = accessed_value;
}}"));
        }

        kernel
    }
}

impl<B> traits::SetDimensionsParam for BufferLat<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    fn set_global_workdimensions_param(&mut self,
                                       dev: &ocl::Device,
                                       _: &ocl::Queue,
                                       _: &ocl::Program) {
        // Needed memory is number of jumps times loop unroll factor that I'll be doing
        let ocl_typ: B = Default::default();
        let element_vector_width = ocl_typ.get_width() as u64;

        let max_mem_alloc = common::get_max_allocatable_mem_device(dev);

        // max_mem_alloc = step * loops * unroll_factor * element_vector_width
        // Other way of saying is that, if self = 1, loops = 1, unroll_factor=2 &
        // element_vector_width = 16, then mem_alloc should be = 32
        let needed_memory = self.unroll_factor * self.step * self.loops * element_vector_width;

        if needed_memory > (max_mem_alloc / self.max_mem_ratio) {
            panic!("Required buffer memory is greater than the device memory. Reduce the \
                    number of unroll_factor");
        }
    }
}

impl<B> traits::GetDimensions for BufferLat<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    fn get_global_workdimensions(&self) -> ocl::SpatialDims {
        self.global_workitems.clone()
    }
}

impl<B> traits::IntoInputBufferParam for BufferLat<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    type Buff = B;

    fn populate_input_buffer_param(&self) -> Vec<Vec<Self::Buff>> {
        // max_mem_alloc = step * loops * unroll_factor * element_vector_width
        // Other way of saying is that, if self = 1, loops = 1, unroll_factor=2 &
        // element_vector_width = 16, then mem_alloc should be = 32
        // element_vector_width is taken care by the data type
        let mem_alloc = self.unroll_factor * self.step * self.loops;

        let buffer: Vec<B> = vec![Default::default(); mem_alloc as usize];
        vec![buffer]
    }

    fn get_input_flags_param(&self) -> ocl::flags::MemFlags {
        let flag: ReadWriteFlags = self.flags[self.current_ind - 1].clone();
        flag.read_flag
    }
}

impl<B> traits::IntoOutputBufferParam for BufferLat<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    type Buff = B;

    fn populate_output_buffer_param(&self) -> Vec<Vec<Self::Buff>> {
        // max_mem_alloc = step * loops * unroll_factor * element_vector_width
        // Other way of saying is that, if self = 1, loops = 1, unroll_factor=2 &
        // element_vector_width = 16, then mem_alloc should be = 32
        // element_vector_width is taken care by the type of element
        let mem_alloc = self.unroll_factor * self.step * self.loops;
        let buffer: Vec<B> = vec![Default::default(); mem_alloc as usize];
        vec![buffer]
    }

    fn get_output_flags_param(&self) -> ocl::flags::MemFlags {
        let flag: ReadWriteFlags = self.flags[self.current_ind - 1].clone();
        flag.write_flag
    }

    fn verify_param(&self, data: Vec<Vec<Self::Buff>>) -> Result<(), String> {
        for datum in data {
            for d in datum {
                let def_value: B = Default::default();
                if def_value != d {
                    return Err(format!("Data should be {:?}, but is {:?}\nVerification \
                                             failed",
                                       def_value,
                                       d));
                }
            }
        }
        Ok(())
    }
}

impl<B> traits::HasStats for BufferLat<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    fn push_time(&mut self, event: &ocl::Event, mp: traits::MeasuringPoint) {
        match mp {
            traits::MeasuringPoint::ReadBuffer => {
                let start_time = match event.profiling_info(ocl::enums::ProfilingInfo::Start) {
                    ocl::enums::ProfilingInfoResult::Start(s) => s,
                    ocl::enums::ProfilingInfoResult::Error(e) => panic!("{:?}", *e),
                    _ => panic!("Match enums properly"),
                };

                let end_time = match event.profiling_info(ocl::enums::ProfilingInfo::End) {
                    ocl::enums::ProfilingInfoResult::End(s) => s,
                    ocl::enums::ProfilingInfoResult::Error(e) => panic!("{:?}", *e),
                    _ => panic!("Match enums properly- end_time"),
                };
                let duration = end_time - start_time;
                self.read_stats.push(duration as f64);
            }

            traits::MeasuringPoint::WriteBuffer => {
                let start_time = match event.profiling_info(ocl::enums::ProfilingInfo::Start) {
                    ocl::enums::ProfilingInfoResult::Start(s) => s,
                    ocl::enums::ProfilingInfoResult::Error(e) => panic!("{:?}", *e),
                    _ => panic!("Match enums properly"),
                };

                let end_time = match event.profiling_info(ocl::enums::ProfilingInfo::End) {
                    ocl::enums::ProfilingInfoResult::End(s) => s,
                    ocl::enums::ProfilingInfoResult::Error(e) => panic!("{:?}", *e),
                    _ => panic!("Match enums properly- end_time"),
                };
                let duration = end_time - start_time;
                self.write_stats.push(duration as f64);
            }

            traits::MeasuringPoint::Execute => {
                let start_time = match event.profiling_info(ocl::enums::ProfilingInfo::Start) {
                    ocl::enums::ProfilingInfoResult::Start(s) => s,
                    ocl::enums::ProfilingInfoResult::Error(e) => panic!("{:?}", *e),
                    _ => panic!("Match enums properly"),
                };

                let end_time = match event.profiling_info(ocl::enums::ProfilingInfo::End) {
                    ocl::enums::ProfilingInfoResult::End(s) => s,
                    ocl::enums::ProfilingInfoResult::Error(e) => panic!("{:?}", *e),
                    _ => panic!("Match enums properly- end_time"),
                };
                let duration = end_time - start_time;
                self.exec_stats.push(duration as f64);
            }
            _ => {}
        }
    }

    fn push_non_event_time(&mut self, time: time::Timespec, mp: traits::MeasuringPoint) {
        match mp {
            traits::MeasuringPoint::Others(0) => {
                self.start_time = time;
            }

            traits::MeasuringPoint::Others(1) => {
                self.end_time = time;
                let duration_ns = (time.sec * 1000_000_000 + time.nsec as i64) -
                                  (self.start_time.sec * 1000_000_000 +
                                   self.start_time.nsec as i64);
                self.entire_stats.push(duration_ns as f64);
            }

            traits::MeasuringPoint::Others(2) => {
                self.b_write_time = time;
            }

            traits::MeasuringPoint::Others(3) => {
                let duration_ns = (time.sec * 1000_000_000 + time.nsec as i64) -
                                  (self.b_write_time.sec * 1000_000_000 +
                                   self.b_write_time.nsec as i64);
                self.b_write_stats.push(duration_ns as f64);
            }

            traits::MeasuringPoint::Others(4) => {
                self.b_read_time = time;
            }

            traits::MeasuringPoint::Others(5) => {
                let duration_ns = (time.sec * 1000_000_000 + time.nsec as i64) -
                                  (self.b_read_time.sec * 1000_000_000 +
                                   self.b_read_time.nsec as i64);
                self.b_read_stats.push(duration_ns as f64);
            }
            _ => {}
        }
    }

    fn clear_stats(&mut self, mp: traits::MeasuringPoint) {
        match mp {
            traits::MeasuringPoint::ReadBuffer => {
                self.read_stats.clear();
            }

            traits::MeasuringPoint::WriteBuffer => {
                self.write_stats.clear();
            }

            traits::MeasuringPoint::Execute => {
                self.exec_stats.clear();
            }

            traits::MeasuringPoint::Others(_) => {
                self.entire_stats.clear();
                self.b_read_stats.clear();
                self.b_write_stats.clear();
            }

        }
    }
}

impl<B> traits::Exp for BufferLat<B>
    where B: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType
{
    fn measure_write_times(&self) -> bool {
        true
    }

    fn measure_read_times(&self) -> bool {
        true
    }

    fn get_header(&self) -> String {
        format!("experiment, type,  unroll_factor, loops, steps, buffer_allocated, ops_performed, \
                 read_flag, write_flag, kernel_exec_time, buffer_write_time, buffer_read_time, \
                 entire_time")
    }

    fn get_stats(&mut self, _: &ocl::Device, _: &ocl::Queue, _: &ocl::Program) -> String {
        let ocl_typ: B = Default::default();

        let read_mean_in_secs;
        if self.flags[self.current_ind - 1].read_flag.contains(ocl::core::MEM_ALLOC_HOST_PTR) {
            read_mean_in_secs = self.b_read_stats.mean() * 1e-9;
        } else {
            read_mean_in_secs = self.read_stats.mean() * 1e-9;
        }

        let write_mean_in_secs;
        if self.flags[self.current_ind - 1].write_flag.contains(ocl::core::MEM_ALLOC_HOST_PTR) {
            write_mean_in_secs = self.b_write_stats.mean() * 1e-9;
        } else {
            write_mean_in_secs = self.write_stats.mean() * 1e-9;
        }
        let exec_mean_in_secs = self.exec_stats.mean() * 1e-9;
        let mem_alloc = ocl_typ.get_width() as u64 * self.unroll_factor * self.loops * self.step;
        let ops_performed = self.loops * self.unroll_factor;
        let entire_time_in_secs = self.entire_stats.mean() * 1e-9;

        format!("BufferLat, {}, {}, {}, {}, {}, {}, {:?}, {:?}, {}, {}, {}, {}",
                ocl_typ.get_opencl_type(),
                self.unroll_factor,
                self.loops,
                self.step,
                mem_alloc,
                ops_performed,
                self.flags[self.current_ind - 1].read_flag,
                self.flags[self.current_ind - 1].write_flag,
                exec_mean_in_secs,
                write_mean_in_secs,
                read_mean_in_secs,
                entire_time_in_secs)
    }

    fn get_experiment_name(&self) -> String {
        let ocl_typ: B = Default::default();
        format!("{}{}", self.experiment_name, ocl_typ.get_opencl_type())
    }
}
