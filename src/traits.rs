use std;
use ocl;
use time;

use ocltypes;

#[allow(dead_code)]
pub enum MeasuringPoint {
    WriteBuffer,
    ReadBuffer,
    Execute,
    Others(u32),
}

pub trait HasKernel {
    fn create_kernel(&self) -> String;
}

pub trait HasKernelParam: std::iter::Iterator {
    fn create_kernel_param(&self) -> String;
}

pub trait SetDimensions {
    fn set_local_workdimensions(&mut self, &ocl::Device, &ocl::Queue, &ocl::Program) {}
    fn set_global_workdimensions(&mut self, &ocl::Device, &ocl::Queue, &ocl::Program);
}

pub trait SetDimensionsParam: std::iter::Iterator {
    fn set_local_workdimensions_param(&mut self, &ocl::Device, &ocl::Queue, &ocl::Program) {}
    fn set_global_workdimensions_param(&mut self, &ocl::Device, &ocl::Queue, &ocl::Program);
}

pub trait GetDimensions {
    fn get_local_workdimensions(&self) -> Option<ocl::SpatialDims> {
        None
    }
    fn get_global_workdimensions(&self) -> ocl::SpatialDims;
}

pub trait IntoInputBuffer: SetDimensions + GetDimensions {
    type Buff: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType;

    fn populate_input_buffer(&self) -> Vec<Vec<Self::Buff>>;
    fn has_scalar(&self) -> bool {
        false
    }
    fn populate_scalar(&self) -> Vec<Self::Buff> {
        Vec::new()
    }
    fn get_input_flags(&self) -> ocl::flags::MemFlags {
        ocl::core::MEM_READ_WRITE | ocl::core::MEM_COPY_HOST_PTR
    }
}

pub trait IntoInputBufferParam: SetDimensionsParam + GetDimensions {
    type Buff: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType;

    fn populate_input_buffer_param(&self) -> Vec<Vec<Self::Buff>>;
    fn has_scalar_param(&self) -> bool {
        false
    }
    fn populate_scalar_param(&self) -> Vec<Self::Buff> {
        Vec::new()
    }
    fn get_input_flags_param(&self) -> ocl::flags::MemFlags {
        ocl::core::MEM_READ_WRITE | ocl::core::MEM_COPY_HOST_PTR
    }
}

pub trait IntoOutputBuffer: SetDimensions + GetDimensions {
    type Buff: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType;

    fn populate_output_buffer(&self) -> Vec<Vec<Self::Buff>>;
    fn read_scalar(&self) -> bool {
        false
    }
    fn get_output_flags(&self) -> ocl::flags::MemFlags {
        ocl::core::MEM_READ_WRITE
    }

    fn verify(&self, Vec<Vec<Self::Buff>>) -> Result<(), String> {
        Ok(())
    }
}

pub trait IntoOutputBufferParam: SetDimensionsParam + GetDimensions {
    type Buff: ocl::traits::OclPrm + ocltypes::HasSize + ocltypes::HasWidth + ocltypes::HasOpenCLType;

    fn populate_output_buffer_param(&self) -> Vec<Vec<Self::Buff>>;
    fn read_scalar_param(&self) -> bool {
        false
    }
    fn get_output_flags_param(&self) -> ocl::flags::MemFlags {
        ocl::core::MEM_READ_WRITE
    }

    fn verify_param(&self, Vec<Vec<Self::Buff>>) -> Result<(), String> {
        Ok(())
    }
}

pub trait HasStats {
    fn push_time(&mut self, &ocl::Event, MeasuringPoint);
    fn push_non_event_time(&mut self, time::Timespec, MeasuringPoint) {}
    fn clear_stats(&mut self, MeasuringPoint);
}

pub trait Exp: HasStats + std::iter::Iterator {
    fn measure_read_times(&self) -> bool {
        false
    }
    fn measure_write_times(&self) -> bool {
        false
    }
    fn get_header(&self) -> String;
    fn get_stats(&mut self, dev: &ocl::Device, que: &ocl::Queue, prog: &ocl::Program) -> String;
    fn get_experiment_name(&self) -> String;
}
