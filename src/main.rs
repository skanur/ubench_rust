extern crate ocl;
extern crate ocl_core;
extern crate time;

use std::io::Write;

mod stats;
mod ocltypes;
mod traits;
mod mwp_traits;
mod startup_traits;
mod buff_read_write;
mod buff_lat;
mod exp;
mod common;

fn main() {
    let platform_id = env!("PLATFORM")
        .parse::<usize>()
        .map_err(|e| ocl::Error::String(e.to_string()))
        .expect("Couldn't parse PLATFORM");
    let device_id = env!("DEVICE")
        .parse::<usize>()
        .map_err(|e| ocl::Error::String(e.to_string()))
        .expect("Couldn't parse DEVICE");
    let iter = env!("MAX_ITERATIONS")
        .parse::<u32>()
        .map_err(|e| ocl::Error::String(e.to_string()))
        .expect("Couldn't parse MAX_ITERATIONS");

    // ************ Startup time experiment *************
    let steps: Vec<u32> = vec![8, 10, 32, 50, 64, 100, 128, 200, 256, 300];
    {
        let startup: startup_traits::StartupTime<i32> = startup_traits::StartupTime::new()
            .set_min_workitems(1)
            .and_then(|m| m.set_max_workitems(64000))
            .and_then(|m| m.set_incr(steps))
            .and_then(|m| m.build())
            .expect("Couldn't build StartupTime experiment");

        let mut exp = exp::ExpType1::new(startup)
            .set_platform_id(platform_id)
            .set_device_id(device_id)
            .set_max_iterations(iter)
            .build();

        match exp.execute() {
            Ok(_) => writeln!(&mut std::io::stderr(), "StartupTime for in is OK").unwrap(),
            Err(e) => {
                writeln!(&mut std::io::stderr(),
                         "StartupTime for int failed with {:?}",
                         e)
                    .unwrap()
            }
        }
    }
}
