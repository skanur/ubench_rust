use ocl;

pub trait HasWidth {
    fn get_width(&self) -> usize;
}

pub trait HasSize {
    fn get_size(&self) -> usize;
}

pub trait HasOpenCLType {
    fn get_opencl_type(&self) -> String;
    fn get_base_type(&self) -> String {
        self.get_opencl_type()
    }
}


impl HasWidth for u8 {
    fn get_width(&self) -> usize {
        1usize
    }
}

impl HasSize for u8 {
    fn get_size(&self) -> usize {
        1usize
    }
}

impl HasOpenCLType for u8 {
    fn get_opencl_type(&self) -> String {
        String::from("uchar")
    }
}

impl HasWidth for i8 {
    fn get_width(&self) -> usize {
        1usize
    }
}

impl HasSize for i8 {
    fn get_size(&self) -> usize {
        1usize
    }
}

impl HasOpenCLType for i8 {
    fn get_opencl_type(&self) -> String {
        String::from("char")
    }
}

impl HasWidth for u16 {
    fn get_width(&self) -> usize {
        1usize
    }
}

impl HasSize for u16 {
    fn get_size(&self) -> usize {
        2usize
    }
}

impl HasOpenCLType for u16 {
    fn get_opencl_type(&self) -> String {
        String::from("ushort")
    }
}

impl HasWidth for i16 {
    fn get_width(&self) -> usize {
        1usize
    }
}

impl HasSize for i16 {
    fn get_size(&self) -> usize {
        2usize
    }
}

impl HasOpenCLType for i16 {
    fn get_opencl_type(&self) -> String {
        String::from("short")
    }
}

impl HasWidth for u32 {
    fn get_width(&self) -> usize {
        1usize
    }
}

impl HasSize for u32 {
    fn get_size(&self) -> usize {
        4usize
    }
}

impl HasOpenCLType for u32 {
    fn get_opencl_type(&self) -> String {
        String::from("uint")
    }
}

impl HasWidth for i32 {
    fn get_width(&self) -> usize {
        1usize
    }
}

impl HasSize for i32 {
    fn get_size(&self) -> usize {
        4usize
    }
}

impl HasOpenCLType for i32 {
    fn get_opencl_type(&self) -> String {
        String::from("int")
    }
}

impl HasWidth for u64 {
    fn get_width(&self) -> usize {
        1usize
    }
}

impl HasSize for u64 {
    fn get_size(&self) -> usize {
        8usize
    }
}

impl HasOpenCLType for u64 {
    fn get_opencl_type(&self) -> String {
        String::from("ulong")
    }
}

impl HasWidth for i64 {
    fn get_width(&self) -> usize {
        1usize
    }
}

impl HasSize for i64 {
    fn get_size(&self) -> usize {
        8usize
    }
}

impl HasOpenCLType for i64 {
    fn get_opencl_type(&self) -> String {
        String::from("long")
    }
}

impl HasWidth for f32 {
    fn get_width(&self) -> usize {
        1usize
    }
}

impl HasSize for f32 {
    fn get_size(&self) -> usize {
        4usize
    }
}

impl HasOpenCLType for f32 {
    fn get_opencl_type(&self) -> String {
        String::from("float")
    }
}

impl HasWidth for f64 {
    fn get_width(&self) -> usize {
        1usize
    }
}

impl HasSize for f64 {
    fn get_size(&self) -> usize {
        8usize
    }
}

impl HasOpenCLType for f64 {
    fn get_opencl_type(&self) -> String {
        String::from("double")
    }
}

impl HasWidth for usize {
    fn get_width(&self) -> usize {
        1usize
    }
}

impl HasSize for usize {
    fn get_size(&self) -> usize {
        8usize
    }
}

impl HasOpenCLType for usize {
    fn get_opencl_type(&self) -> String {
        String::from("size")
    }
}

impl HasWidth for isize {
    fn get_width(&self) -> usize {
        1usize
    }
}

impl HasSize for isize {
    fn get_size(&self) -> usize {
        8usize
    }
}

impl HasOpenCLType for isize {
    fn get_opencl_type(&self) -> String {
        String::from("size")
    }
}


impl HasWidth for ocl::aliases::ClChar2 {
    fn get_width(&self) -> usize {
        2usize
    }
}

impl HasWidth for ocl::aliases::ClChar3 {
    fn get_width(&self) -> usize {
        3usize
    }
}

impl HasWidth for ocl::aliases::ClChar4 {
    fn get_width(&self) -> usize {
        4usize
    }
}

impl HasWidth for ocl::aliases::ClChar8 {
    fn get_width(&self) -> usize {
        8usize
    }
}

impl HasWidth for ocl::aliases::ClChar16 {
    fn get_width(&self) -> usize {
        16usize
    }
}

impl HasWidth for ocl::aliases::ClDouble2 {
    fn get_width(&self) -> usize {
        2usize
    }
}

impl HasWidth for ocl::aliases::ClDouble3 {
    fn get_width(&self) -> usize {
        3usize
    }
}

impl HasWidth for ocl::aliases::ClDouble4 {
    fn get_width(&self) -> usize {
        4usize
    }
}

impl HasWidth for ocl::aliases::ClDouble8 {
    fn get_width(&self) -> usize {
        8usize
    }
}

impl HasWidth for ocl::aliases::ClDouble16 {
    fn get_width(&self) -> usize {
        16usize
    }
}

impl HasWidth for ocl::aliases::ClFloat2 {
    fn get_width(&self) -> usize {
        2usize
    }
}

impl HasWidth for ocl::aliases::ClFloat3 {
    fn get_width(&self) -> usize {
        3usize
    }
}

impl HasWidth for ocl::aliases::ClFloat4 {
    fn get_width(&self) -> usize {
        4usize
    }
}

impl HasWidth for ocl::aliases::ClFloat8 {
    fn get_width(&self) -> usize {
        8usize
    }
}

impl HasWidth for ocl::aliases::ClFloat16 {
    fn get_width(&self) -> usize {
        16usize
    }
}

impl HasWidth for ocl::aliases::ClInt2 {
    fn get_width(&self) -> usize {
        2usize
    }
}

impl HasWidth for ocl::aliases::ClInt3 {
    fn get_width(&self) -> usize {
        3usize
    }
}

impl HasWidth for ocl::aliases::ClInt4 {
    fn get_width(&self) -> usize {
        4usize
    }
}

impl HasWidth for ocl::aliases::ClInt8 {
    fn get_width(&self) -> usize {
        8usize
    }
}

impl HasWidth for ocl::aliases::ClInt16 {
    fn get_width(&self) -> usize {
        16usize
    }
}

impl HasWidth for ocl::aliases::ClLong1 {
    fn get_width(&self) -> usize {
        1usize
    }
}

impl HasWidth for ocl::aliases::ClLong2 {
    fn get_width(&self) -> usize {
        2usize
    }
}

impl HasWidth for ocl::aliases::ClLong3 {
    fn get_width(&self) -> usize {
        3usize
    }
}

impl HasWidth for ocl::aliases::ClLong4 {
    fn get_width(&self) -> usize {
        4usize
    }
}

impl HasWidth for ocl::aliases::ClLong8 {
    fn get_width(&self) -> usize {
        8usize
    }
}

impl HasWidth for ocl::aliases::ClLong16 {
    fn get_width(&self) -> usize {
        16usize
    }
}

impl HasWidth for ocl::aliases::ClShort2 {
    fn get_width(&self) -> usize {
        2usize
    }
}

impl HasWidth for ocl::aliases::ClShort3 {
    fn get_width(&self) -> usize {
        3usize
    }
}

impl HasWidth for ocl::aliases::ClShort4 {
    fn get_width(&self) -> usize {
        4usize
    }
}

impl HasWidth for ocl::aliases::ClShort8 {
    fn get_width(&self) -> usize {
        8usize
    }
}

impl HasWidth for ocl::aliases::ClShort16 {
    fn get_width(&self) -> usize {
        16usize
    }
}

impl HasWidth for ocl::aliases::ClUchar2 {
    fn get_width(&self) -> usize {
        2usize
    }
}

impl HasWidth for ocl::aliases::ClUchar3 {
    fn get_width(&self) -> usize {
        3usize
    }
}

impl HasWidth for ocl::aliases::ClUchar4 {
    fn get_width(&self) -> usize {
        4usize
    }
}

impl HasWidth for ocl::aliases::ClUchar8 {
    fn get_width(&self) -> usize {
        8usize
    }
}

impl HasWidth for ocl::aliases::ClUchar16 {
    fn get_width(&self) -> usize {
        16usize
    }
}

impl HasWidth for ocl::aliases::ClUint2 {
    fn get_width(&self) -> usize {
        2usize
    }
}

impl HasWidth for ocl::aliases::ClUint3 {
    fn get_width(&self) -> usize {
        3usize
    }
}

impl HasWidth for ocl::aliases::ClUint4 {
    fn get_width(&self) -> usize {
        4usize
    }
}

impl HasWidth for ocl::aliases::ClUint8 {
    fn get_width(&self) -> usize {
        8usize
    }
}

impl HasWidth for ocl::aliases::ClUint16 {
    fn get_width(&self) -> usize {
        16usize
    }
}

impl HasWidth for ocl::aliases::ClUlong1 {
    fn get_width(&self) -> usize {
        1usize
    }
}

impl HasWidth for ocl::aliases::ClUlong2 {
    fn get_width(&self) -> usize {
        2usize
    }
}

impl HasWidth for ocl::aliases::ClUlong3 {
    fn get_width(&self) -> usize {
        3usize
    }
}

impl HasWidth for ocl::aliases::ClUlong4 {
    fn get_width(&self) -> usize {
        4usize
    }
}

impl HasWidth for ocl::aliases::ClUlong8 {
    fn get_width(&self) -> usize {
        8usize
    }
}

impl HasWidth for ocl::aliases::ClUlong16 {
    fn get_width(&self) -> usize {
        16usize
    }
}

impl HasWidth for ocl::aliases::ClUshort2 {
    fn get_width(&self) -> usize {
        2usize
    }
}

impl HasWidth for ocl::aliases::ClUshort3 {
    fn get_width(&self) -> usize {
        3usize
    }
}

impl HasWidth for ocl::aliases::ClUshort4 {
    fn get_width(&self) -> usize {
        4usize
    }
}

impl HasWidth for ocl::aliases::ClUshort8 {
    fn get_width(&self) -> usize {
        8usize
    }
}

impl HasWidth for ocl::aliases::ClUshort16 {
    fn get_width(&self) -> usize {
        16usize
    }
}

// HasSize implementation for OclVec types

impl HasSize for ocl::aliases::ClChar2 {
    fn get_size(&self) -> usize {
        2usize * 1usize
    }
}

impl HasSize for ocl::aliases::ClChar3 {
    fn get_size(&self) -> usize {
        3usize * 1usize
    }
}

impl HasSize for ocl::aliases::ClChar4 {
    fn get_size(&self) -> usize {
        4usize * 1usize
    }
}

impl HasSize for ocl::aliases::ClChar8 {
    fn get_size(&self) -> usize {
        8usize * 1usize
    }
}

impl HasSize for ocl::aliases::ClChar16 {
    fn get_size(&self) -> usize {
        16usize * 1usize
    }
}

impl HasSize for ocl::aliases::ClDouble2 {
    fn get_size(&self) -> usize {
        2usize * 8usize
    }
}

impl HasSize for ocl::aliases::ClDouble3 {
    fn get_size(&self) -> usize {
        3usize * 8usize
    }
}

impl HasSize for ocl::aliases::ClDouble4 {
    fn get_size(&self) -> usize {
        4usize * 8usize
    }
}

impl HasSize for ocl::aliases::ClDouble8 {
    fn get_size(&self) -> usize {
        8usize * 8usize
    }
}

impl HasSize for ocl::aliases::ClDouble16 {
    fn get_size(&self) -> usize {
        16usize * 8usize
    }
}

impl HasSize for ocl::aliases::ClFloat2 {
    fn get_size(&self) -> usize {
        2usize * 4usize
    }
}

impl HasSize for ocl::aliases::ClFloat3 {
    fn get_size(&self) -> usize {
        3usize * 4usize
    }
}

impl HasSize for ocl::aliases::ClFloat4 {
    fn get_size(&self) -> usize {
        4usize * 4usize
    }
}

impl HasSize for ocl::aliases::ClFloat8 {
    fn get_size(&self) -> usize {
        8usize * 4usize
    }
}

impl HasSize for ocl::aliases::ClFloat16 {
    fn get_size(&self) -> usize {
        16usize * 4usize
    }
}

impl HasSize for ocl::aliases::ClInt2 {
    fn get_size(&self) -> usize {
        2usize * 4usize
    }
}

impl HasSize for ocl::aliases::ClInt3 {
    fn get_size(&self) -> usize {
        3usize * 4usize
    }
}

impl HasSize for ocl::aliases::ClInt4 {
    fn get_size(&self) -> usize {
        4usize * 4usize
    }
}

impl HasSize for ocl::aliases::ClInt8 {
    fn get_size(&self) -> usize {
        8usize * 4usize
    }
}

impl HasSize for ocl::aliases::ClInt16 {
    fn get_size(&self) -> usize {
        16usize * 4usize
    }
}

impl HasSize for ocl::aliases::ClLong1 {
    fn get_size(&self) -> usize {
        1usize * 8usize
    }
}

impl HasSize for ocl::aliases::ClLong2 {
    fn get_size(&self) -> usize {
        2usize * 8usize
    }
}

impl HasSize for ocl::aliases::ClLong3 {
    fn get_size(&self) -> usize {
        3usize * 8usize
    }
}

impl HasSize for ocl::aliases::ClLong4 {
    fn get_size(&self) -> usize {
        4usize * 8usize
    }
}

impl HasSize for ocl::aliases::ClLong8 {
    fn get_size(&self) -> usize {
        8usize * 8usize
    }
}

impl HasSize for ocl::aliases::ClLong16 {
    fn get_size(&self) -> usize {
        16usize * 8usize
    }
}

impl HasSize for ocl::aliases::ClShort2 {
    fn get_size(&self) -> usize {
        2usize * 2usize
    }
}

impl HasSize for ocl::aliases::ClShort3 {
    fn get_size(&self) -> usize {
        3usize * 2usize
    }
}

impl HasSize for ocl::aliases::ClShort4 {
    fn get_size(&self) -> usize {
        4usize * 2usize
    }
}

impl HasSize for ocl::aliases::ClShort8 {
    fn get_size(&self) -> usize {
        8usize * 2usize
    }
}

impl HasSize for ocl::aliases::ClShort16 {
    fn get_size(&self) -> usize {
        16usize * 2usize
    }
}

impl HasSize for ocl::aliases::ClUchar2 {
    fn get_size(&self) -> usize {
        2usize * 1usize
    }
}

impl HasSize for ocl::aliases::ClUchar3 {
    fn get_size(&self) -> usize {
        3usize * 1usize
    }
}

impl HasSize for ocl::aliases::ClUchar4 {
    fn get_size(&self) -> usize {
        4usize * 1usize
    }
}

impl HasSize for ocl::aliases::ClUchar8 {
    fn get_size(&self) -> usize {
        8usize * 1usize
    }
}

impl HasSize for ocl::aliases::ClUchar16 {
    fn get_size(&self) -> usize {
        16usize * 1usize
    }
}

impl HasSize for ocl::aliases::ClUint2 {
    fn get_size(&self) -> usize {
        2usize * 4usize
    }
}

impl HasSize for ocl::aliases::ClUint3 {
    fn get_size(&self) -> usize {
        3usize * 4usize
    }
}

impl HasSize for ocl::aliases::ClUint4 {
    fn get_size(&self) -> usize {
        4usize * 4usize
    }
}

impl HasSize for ocl::aliases::ClUint8 {
    fn get_size(&self) -> usize {
        8usize * 4usize
    }
}

impl HasSize for ocl::aliases::ClUint16 {
    fn get_size(&self) -> usize {
        16usize * 4usize
    }
}

impl HasSize for ocl::aliases::ClUlong1 {
    fn get_size(&self) -> usize {
        1usize * 8usize
    }
}

impl HasSize for ocl::aliases::ClUlong2 {
    fn get_size(&self) -> usize {
        2usize * 8usize
    }
}

impl HasSize for ocl::aliases::ClUlong3 {
    fn get_size(&self) -> usize {
        3usize * 8usize
    }
}

impl HasSize for ocl::aliases::ClUlong4 {
    fn get_size(&self) -> usize {
        4usize * 8usize
    }
}

impl HasSize for ocl::aliases::ClUlong8 {
    fn get_size(&self) -> usize {
        8usize * 8usize
    }
}

impl HasSize for ocl::aliases::ClUlong16 {
    fn get_size(&self) -> usize {
        16usize * 8usize
    }
}

impl HasSize for ocl::aliases::ClUshort2 {
    fn get_size(&self) -> usize {
        2usize * 2usize
    }
}

impl HasSize for ocl::aliases::ClUshort3 {
    fn get_size(&self) -> usize {
        3usize * 2usize
    }
}

impl HasSize for ocl::aliases::ClUshort4 {
    fn get_size(&self) -> usize {
        4usize * 2usize
    }
}

impl HasSize for ocl::aliases::ClUshort8 {
    fn get_size(&self) -> usize {
        8usize * 2usize
    }
}

impl HasSize for ocl::aliases::ClUshort16 {
    fn get_size(&self) -> usize {
        16usize * 2usize
    }
}

// HasOpenCLString

impl HasOpenCLType for ocl::aliases::ClChar2 {
    fn get_opencl_type(&self) -> String {
        String::from("char2")
    }

    fn get_base_type(&self) -> String {
        String::from("char")
    }
}

impl HasOpenCLType for ocl::aliases::ClChar3 {
    fn get_opencl_type(&self) -> String {
        String::from("char3")
    }

    fn get_base_type(&self) -> String {
        String::from("char")
    }
}

impl HasOpenCLType for ocl::aliases::ClChar4 {
    fn get_opencl_type(&self) -> String {
        String::from("char4")
    }

    fn get_base_type(&self) -> String {
        String::from("char")
    }
}

impl HasOpenCLType for ocl::aliases::ClChar8 {
    fn get_opencl_type(&self) -> String {
        String::from("char8")
    }

    fn get_base_type(&self) -> String {
        String::from("char")
    }
}

impl HasOpenCLType for ocl::aliases::ClChar16 {
    fn get_opencl_type(&self) -> String {
        String::from("char16")
    }

    fn get_base_type(&self) -> String {
        String::from("char")
    }
}

impl HasOpenCLType for ocl::aliases::ClDouble2 {
    fn get_opencl_type(&self) -> String {
        String::from("double2")
    }

    fn get_base_type(&self) -> String {
        String::from("double")
    }
}

impl HasOpenCLType for ocl::aliases::ClDouble3 {
    fn get_opencl_type(&self) -> String {
        String::from("double3")
    }

    fn get_base_type(&self) -> String {
        String::from("double")
    }
}

impl HasOpenCLType for ocl::aliases::ClDouble4 {
    fn get_opencl_type(&self) -> String {
        String::from("double4")
    }

    fn get_base_type(&self) -> String {
        String::from("double")
    }
}

impl HasOpenCLType for ocl::aliases::ClDouble8 {
    fn get_opencl_type(&self) -> String {
        String::from("double8")
    }

    fn get_base_type(&self) -> String {
        String::from("double")
    }
}

impl HasOpenCLType for ocl::aliases::ClDouble16 {
    fn get_opencl_type(&self) -> String {
        String::from("double16")
    }

    fn get_base_type(&self) -> String {
        String::from("double")
    }
}

impl HasOpenCLType for ocl::aliases::ClFloat2 {
    fn get_opencl_type(&self) -> String {
        String::from("float2")
    }

    fn get_base_type(&self) -> String {
        String::from("float")
    }
}

impl HasOpenCLType for ocl::aliases::ClFloat3 {
    fn get_opencl_type(&self) -> String {
        String::from("float3")
    }

    fn get_base_type(&self) -> String {
        String::from("float")
    }
}

impl HasOpenCLType for ocl::aliases::ClFloat4 {
    fn get_opencl_type(&self) -> String {
        String::from("float4")
    }

    fn get_base_type(&self) -> String {
        String::from("float")
    }
}

impl HasOpenCLType for ocl::aliases::ClFloat8 {
    fn get_opencl_type(&self) -> String {
        String::from("float8")
    }

    fn get_base_type(&self) -> String {
        String::from("float")
    }
}

impl HasOpenCLType for ocl::aliases::ClFloat16 {
    fn get_opencl_type(&self) -> String {
        String::from("float16")
    }

    fn get_base_type(&self) -> String {
        String::from("float")
    }
}

impl HasOpenCLType for ocl::aliases::ClInt2 {
    fn get_opencl_type(&self) -> String {
        String::from("int2")
    }

    fn get_base_type(&self) -> String {
        String::from("int")
    }
}

impl HasOpenCLType for ocl::aliases::ClInt3 {
    fn get_opencl_type(&self) -> String {
        String::from("int3")
    }

    fn get_base_type(&self) -> String {
        String::from("int")
    }
}

impl HasOpenCLType for ocl::aliases::ClInt4 {
    fn get_opencl_type(&self) -> String {
        String::from("int4")
    }

    fn get_base_type(&self) -> String {
        String::from("int")
    }
}

impl HasOpenCLType for ocl::aliases::ClInt8 {
    fn get_opencl_type(&self) -> String {
        String::from("int8")
    }

    fn get_base_type(&self) -> String {
        String::from("int")
    }
}

impl HasOpenCLType for ocl::aliases::ClInt16 {
    fn get_opencl_type(&self) -> String {
        String::from("int16")
    }

    fn get_base_type(&self) -> String {
        String::from("int")
    }
}

impl HasOpenCLType for ocl::aliases::ClLong1 {
    fn get_opencl_type(&self) -> String {
        String::from("long")
    }

    fn get_base_type(&self) -> String {
        String::from("long")
    }
}

impl HasOpenCLType for ocl::aliases::ClLong2 {
    fn get_opencl_type(&self) -> String {
        String::from("long2")
    }

    fn get_base_type(&self) -> String {
        String::from("long")
    }
}

impl HasOpenCLType for ocl::aliases::ClLong3 {
    fn get_opencl_type(&self) -> String {
        String::from("long3")
    }

    fn get_base_type(&self) -> String {
        String::from("long")
    }
}

impl HasOpenCLType for ocl::aliases::ClLong4 {
    fn get_opencl_type(&self) -> String {
        String::from("long4")
    }

    fn get_base_type(&self) -> String {
        String::from("long")
    }
}

impl HasOpenCLType for ocl::aliases::ClLong8 {
    fn get_opencl_type(&self) -> String {
        String::from("long8")
    }

    fn get_base_type(&self) -> String {
        String::from("long")
    }
}

impl HasOpenCLType for ocl::aliases::ClLong16 {
    fn get_opencl_type(&self) -> String {
        String::from("long16")
    }

    fn get_base_type(&self) -> String {
        String::from("long")
    }
}

impl HasOpenCLType for ocl::aliases::ClShort2 {
    fn get_opencl_type(&self) -> String {
        String::from("short2")
    }

    fn get_base_type(&self) -> String {
        String::from("short")
    }
}

impl HasOpenCLType for ocl::aliases::ClShort3 {
    fn get_opencl_type(&self) -> String {
        String::from("short3")
    }

    fn get_base_type(&self) -> String {
        String::from("short")
    }
}

impl HasOpenCLType for ocl::aliases::ClShort4 {
    fn get_opencl_type(&self) -> String {
        String::from("short4")
    }

    fn get_base_type(&self) -> String {
        String::from("short")
    }
}

impl HasOpenCLType for ocl::aliases::ClShort8 {
    fn get_opencl_type(&self) -> String {
        String::from("short8")
    }

    fn get_base_type(&self) -> String {
        String::from("short")
    }
}

impl HasOpenCLType for ocl::aliases::ClShort16 {
    fn get_opencl_type(&self) -> String {
        String::from("short16")
    }

    fn get_base_type(&self) -> String {
        String::from("short")
    }
}

impl HasOpenCLType for ocl::aliases::ClUchar2 {
    fn get_opencl_type(&self) -> String {
        String::from("uchar2")
    }

    fn get_base_type(&self) -> String {
        String::from("uchar")
    }
}

impl HasOpenCLType for ocl::aliases::ClUchar3 {
    fn get_opencl_type(&self) -> String {
        String::from("uchar3")
    }

    fn get_base_type(&self) -> String {
        String::from("uchar")
    }
}

impl HasOpenCLType for ocl::aliases::ClUchar4 {
    fn get_opencl_type(&self) -> String {
        String::from("uchar4")
    }

    fn get_base_type(&self) -> String {
        String::from("uchar")
    }
}

impl HasOpenCLType for ocl::aliases::ClUchar8 {
    fn get_opencl_type(&self) -> String {
        String::from("uchar8")
    }

    fn get_base_type(&self) -> String {
        String::from("uchar")
    }
}

impl HasOpenCLType for ocl::aliases::ClUchar16 {
    fn get_opencl_type(&self) -> String {
        String::from("uchar16")
    }

    fn get_base_type(&self) -> String {
        String::from("uchar")
    }
}

impl HasOpenCLType for ocl::aliases::ClUint2 {
    fn get_opencl_type(&self) -> String {
        String::from("uint2")
    }

    fn get_base_type(&self) -> String {
        String::from("uint")
    }
}

impl HasOpenCLType for ocl::aliases::ClUint3 {
    fn get_opencl_type(&self) -> String {
        String::from("uint3")
    }

    fn get_base_type(&self) -> String {
        String::from("uint")
    }
}

impl HasOpenCLType for ocl::aliases::ClUint4 {
    fn get_opencl_type(&self) -> String {
        String::from("uint4")
    }

    fn get_base_type(&self) -> String {
        String::from("uint")
    }
}

impl HasOpenCLType for ocl::aliases::ClUint8 {
    fn get_opencl_type(&self) -> String {
        String::from("uint8")
    }

    fn get_base_type(&self) -> String {
        String::from("uint")
    }
}

impl HasOpenCLType for ocl::aliases::ClUint16 {
    fn get_opencl_type(&self) -> String {
        String::from("uint16")
    }

    fn get_base_type(&self) -> String {
        String::from("uint")
    }
}

impl HasOpenCLType for ocl::aliases::ClUlong1 {
    fn get_opencl_type(&self) -> String {
        String::from("ulong")
    }

    fn get_base_type(&self) -> String {
        String::from("ulong")
    }
}

impl HasOpenCLType for ocl::aliases::ClUlong2 {
    fn get_opencl_type(&self) -> String {
        String::from("ulong2")
    }

    fn get_base_type(&self) -> String {
        String::from("ulong")
    }
}

impl HasOpenCLType for ocl::aliases::ClUlong3 {
    fn get_opencl_type(&self) -> String {
        String::from("ulong3")
    }

    fn get_base_type(&self) -> String {
        String::from("ulong")
    }
}

impl HasOpenCLType for ocl::aliases::ClUlong4 {
    fn get_opencl_type(&self) -> String {
        String::from("ulong4")
    }

    fn get_base_type(&self) -> String {
        String::from("ulong")
    }
}

impl HasOpenCLType for ocl::aliases::ClUlong8 {
    fn get_opencl_type(&self) -> String {
        String::from("ulong8")
    }

    fn get_base_type(&self) -> String {
        String::from("ulong")
    }
}

impl HasOpenCLType for ocl::aliases::ClUlong16 {
    fn get_opencl_type(&self) -> String {
        String::from("ulong16")
    }

    fn get_base_type(&self) -> String {
        String::from("ulong")
    }
}

impl HasOpenCLType for ocl::aliases::ClUshort2 {
    fn get_opencl_type(&self) -> String {
        String::from("ushort2")
    }

    fn get_base_type(&self) -> String {
        String::from("ushort")
    }
}

impl HasOpenCLType for ocl::aliases::ClUshort3 {
    fn get_opencl_type(&self) -> String {
        String::from("ushort3")
    }

    fn get_base_type(&self) -> String {
        String::from("ushort")
    }
}

impl HasOpenCLType for ocl::aliases::ClUshort4 {
    fn get_opencl_type(&self) -> String {
        String::from("ushort4")
    }

    fn get_base_type(&self) -> String {
        String::from("ushort")
    }
}

impl HasOpenCLType for ocl::aliases::ClUshort8 {
    fn get_opencl_type(&self) -> String {
        String::from("ushort8")
    }

    fn get_base_type(&self) -> String {
        String::from("ushort")
    }
}

impl HasOpenCLType for ocl::aliases::ClUshort16 {
    fn get_opencl_type(&self) -> String {
        String::from("ushort16")
    }

    fn get_base_type(&self) -> String {
        String::from("ushort")
    }
}


#[test]
fn test_vector_width() {
    let val: ocl::aliases::ClChar2 = Default::default();
    assert_eq!(val.get_width(), 2);

    let val: ocl::aliases::ClChar3 = Default::default();
    assert_eq!(val.get_width(), 3);

    let val: ocl::aliases::ClChar4 = Default::default();
    assert_eq!(val.get_width(), 4);

    let val: ocl::aliases::ClChar8 = Default::default();
    assert_eq!(val.get_width(), 8);

    let val: ocl::aliases::ClChar16 = Default::default();
    assert_eq!(val.get_width(), 16);

    let val: ocl::aliases::ClDouble2 = Default::default();
    assert_eq!(val.get_width(), 2);

    let val: ocl::aliases::ClDouble3 = Default::default();
    assert_eq!(val.get_width(), 3);

    let val: ocl::aliases::ClDouble4 = Default::default();
    assert_eq!(val.get_width(), 4);

    let val: ocl::aliases::ClDouble8 = Default::default();
    assert_eq!(val.get_width(), 8);

    let val: ocl::aliases::ClDouble16 = Default::default();
    assert_eq!(val.get_width(), 16);

    let val: ocl::aliases::ClFloat2 = Default::default();
    assert_eq!(val.get_width(), 2);

    let val: ocl::aliases::ClFloat3 = Default::default();
    assert_eq!(val.get_width(), 3);

    let val: ocl::aliases::ClFloat4 = Default::default();
    assert_eq!(val.get_width(), 4);

    let val: ocl::aliases::ClFloat8 = Default::default();
    assert_eq!(val.get_width(), 8);

    let val: ocl::aliases::ClFloat16 = Default::default();
    assert_eq!(val.get_width(), 16);

    let val: ocl::aliases::ClInt2 = Default::default();
    assert_eq!(val.get_width(), 2);

    let val: ocl::aliases::ClInt3 = Default::default();
    assert_eq!(val.get_width(), 3);

    let val: ocl::aliases::ClInt4 = Default::default();
    assert_eq!(val.get_width(), 4);

    let val: ocl::aliases::ClInt8 = Default::default();
    assert_eq!(val.get_width(), 8);

    let val: ocl::aliases::ClInt16 = Default::default();
    assert_eq!(val.get_width(), 16);

    let val: ocl::aliases::ClLong1 = Default::default();
    assert_eq!(val.get_width(), 1);

    let val: ocl::aliases::ClLong2 = Default::default();
    assert_eq!(val.get_width(), 2);

    let val: ocl::aliases::ClLong3 = Default::default();
    assert_eq!(val.get_width(), 3);

    let val: ocl::aliases::ClLong4 = Default::default();
    assert_eq!(val.get_width(), 4);

    let val: ocl::aliases::ClLong8 = Default::default();
    assert_eq!(val.get_width(), 8);

    let val: ocl::aliases::ClLong16 = Default::default();
    assert_eq!(val.get_width(), 16);

    let val: ocl::aliases::ClShort2 = Default::default();
    assert_eq!(val.get_width(), 2);

    let val: ocl::aliases::ClShort3 = Default::default();
    assert_eq!(val.get_width(), 3);

    let val: ocl::aliases::ClShort4 = Default::default();
    assert_eq!(val.get_width(), 4);

    let val: ocl::aliases::ClShort8 = Default::default();
    assert_eq!(val.get_width(), 8);

    let val: ocl::aliases::ClShort16 = Default::default();
    assert_eq!(val.get_width(), 16);

    let val: ocl::aliases::ClUchar2 = Default::default();
    assert_eq!(val.get_width(), 2);

    let val: ocl::aliases::ClUchar3 = Default::default();
    assert_eq!(val.get_width(), 3);

    let val: ocl::aliases::ClUchar4 = Default::default();
    assert_eq!(val.get_width(), 4);

    let val: ocl::aliases::ClUchar8 = Default::default();
    assert_eq!(val.get_width(), 8);

    let val: ocl::aliases::ClUchar16 = Default::default();
    assert_eq!(val.get_width(), 16);

    let val: ocl::aliases::ClUint2 = Default::default();
    assert_eq!(val.get_width(), 2);

    let val: ocl::aliases::ClUint3 = Default::default();
    assert_eq!(val.get_width(), 3);

    let val: ocl::aliases::ClUint4 = Default::default();
    assert_eq!(val.get_width(), 4);

    let val: ocl::aliases::ClUint8 = Default::default();
    assert_eq!(val.get_width(), 8);

    let val: ocl::aliases::ClUint16 = Default::default();
    assert_eq!(val.get_width(), 16);

    let val: ocl::aliases::ClUlong1 = Default::default();
    assert_eq!(val.get_width(), 1);

    let val: ocl::aliases::ClUlong2 = Default::default();
    assert_eq!(val.get_width(), 2);

    let val: ocl::aliases::ClUlong3 = Default::default();
    assert_eq!(val.get_width(), 3);

    let val: ocl::aliases::ClUlong4 = Default::default();
    assert_eq!(val.get_width(), 4);

    let val: ocl::aliases::ClUlong8 = Default::default();
    assert_eq!(val.get_width(), 8);

    let val: ocl::aliases::ClUlong16 = Default::default();
    assert_eq!(val.get_width(), 16);

    let val: ocl::aliases::ClUshort2 = Default::default();
    assert_eq!(val.get_width(), 2);

    let val: ocl::aliases::ClUshort3 = Default::default();
    assert_eq!(val.get_width(), 3);

    let val: ocl::aliases::ClUshort4 = Default::default();
    assert_eq!(val.get_width(), 4);

    let val: ocl::aliases::ClUshort8 = Default::default();
    assert_eq!(val.get_width(), 8);

    let val: ocl::aliases::ClUshort16 = Default::default();
    assert_eq!(val.get_width(), 16);
}

#[test]
fn test_vector_size() {
    let val: ocl::aliases::ClChar2 = Default::default();
    assert_eq!(val.get_size(), 2);

    let val: ocl::aliases::ClChar3 = Default::default();
    assert_eq!(val.get_size(), 3);

    let val: ocl::aliases::ClChar4 = Default::default();
    assert_eq!(val.get_size(), 4);

    let val: ocl::aliases::ClChar8 = Default::default();
    assert_eq!(val.get_size(), 8);

    let val: ocl::aliases::ClChar16 = Default::default();
    assert_eq!(val.get_size(), 16);

    let val: ocl::aliases::ClDouble2 = Default::default();
    assert_eq!(val.get_size(), 2 * 8);

    let val: ocl::aliases::ClDouble3 = Default::default();
    assert_eq!(val.get_size(), 3 * 8);

    let val: ocl::aliases::ClDouble4 = Default::default();
    assert_eq!(val.get_size(), 4 * 8);

    let val: ocl::aliases::ClDouble8 = Default::default();
    assert_eq!(val.get_size(), 8 * 8);

    let val: ocl::aliases::ClDouble16 = Default::default();
    assert_eq!(val.get_size(), 16 * 8);

    let val: ocl::aliases::ClFloat2 = Default::default();
    assert_eq!(val.get_size(), 2 * 4);

    let val: ocl::aliases::ClFloat3 = Default::default();
    assert_eq!(val.get_size(), 3 * 4);

    let val: ocl::aliases::ClFloat4 = Default::default();
    assert_eq!(val.get_size(), 4 * 4);

    let val: ocl::aliases::ClFloat8 = Default::default();
    assert_eq!(val.get_size(), 8 * 4);

    let val: ocl::aliases::ClFloat16 = Default::default();
    assert_eq!(val.get_size(), 16 * 4);

    let val: ocl::aliases::ClInt2 = Default::default();
    assert_eq!(val.get_size(), 2 * 4);

    let val: ocl::aliases::ClInt3 = Default::default();
    assert_eq!(val.get_size(), 3 * 4);

    let val: ocl::aliases::ClInt4 = Default::default();
    assert_eq!(val.get_size(), 4 * 4);

    let val: ocl::aliases::ClInt8 = Default::default();
    assert_eq!(val.get_size(), 8 * 4);

    let val: ocl::aliases::ClInt16 = Default::default();
    assert_eq!(val.get_size(), 16 * 4);

    let val: ocl::aliases::ClLong1 = Default::default();
    assert_eq!(val.get_size(), 1 * 8);

    let val: ocl::aliases::ClLong2 = Default::default();
    assert_eq!(val.get_size(), 2 * 8);

    let val: ocl::aliases::ClLong3 = Default::default();
    assert_eq!(val.get_size(), 3 * 8);

    let val: ocl::aliases::ClLong4 = Default::default();
    assert_eq!(val.get_size(), 4 * 8);

    let val: ocl::aliases::ClLong8 = Default::default();
    assert_eq!(val.get_size(), 8 * 8);

    let val: ocl::aliases::ClLong16 = Default::default();
    assert_eq!(val.get_size(), 16 * 8);

    let val: ocl::aliases::ClShort2 = Default::default();
    assert_eq!(val.get_size(), 2 * 2);

    let val: ocl::aliases::ClShort3 = Default::default();
    assert_eq!(val.get_size(), 3 * 2);

    let val: ocl::aliases::ClShort4 = Default::default();
    assert_eq!(val.get_size(), 4 * 2);

    let val: ocl::aliases::ClShort8 = Default::default();
    assert_eq!(val.get_size(), 8 * 2);

    let val: ocl::aliases::ClShort16 = Default::default();
    assert_eq!(val.get_size(), 16 * 2);

    let val: ocl::aliases::ClUchar2 = Default::default();
    assert_eq!(val.get_size(), 2);

    let val: ocl::aliases::ClUchar3 = Default::default();
    assert_eq!(val.get_size(), 3);

    let val: ocl::aliases::ClUchar4 = Default::default();
    assert_eq!(val.get_size(), 4);

    let val: ocl::aliases::ClUchar8 = Default::default();
    assert_eq!(val.get_size(), 8);

    let val: ocl::aliases::ClUchar16 = Default::default();
    assert_eq!(val.get_size(), 16);

    let val: ocl::aliases::ClUint2 = Default::default();
    assert_eq!(val.get_size(), 2 * 4);

    let val: ocl::aliases::ClUint3 = Default::default();
    assert_eq!(val.get_size(), 3 * 4);

    let val: ocl::aliases::ClUint4 = Default::default();
    assert_eq!(val.get_size(), 4 * 4);

    let val: ocl::aliases::ClUint8 = Default::default();
    assert_eq!(val.get_size(), 8 * 4);

    let val: ocl::aliases::ClUint16 = Default::default();
    assert_eq!(val.get_size(), 16 * 4);

    let val: ocl::aliases::ClUlong1 = Default::default();
    assert_eq!(val.get_size(), 1 * 8);

    let val: ocl::aliases::ClUlong2 = Default::default();
    assert_eq!(val.get_size(), 2 * 8);

    let val: ocl::aliases::ClUlong3 = Default::default();
    assert_eq!(val.get_size(), 3 * 8);

    let val: ocl::aliases::ClUlong4 = Default::default();
    assert_eq!(val.get_size(), 4 * 8);

    let val: ocl::aliases::ClUlong8 = Default::default();
    assert_eq!(val.get_size(), 8 * 8);

    let val: ocl::aliases::ClUlong16 = Default::default();
    assert_eq!(val.get_size(), 16 * 8);

    let val: ocl::aliases::ClUshort2 = Default::default();
    assert_eq!(val.get_size(), 2 * 2);

    let val: ocl::aliases::ClUshort3 = Default::default();
    assert_eq!(val.get_size(), 3 * 2);

    let val: ocl::aliases::ClUshort4 = Default::default();
    assert_eq!(val.get_size(), 4 * 2);

    let val: ocl::aliases::ClUshort8 = Default::default();
    assert_eq!(val.get_size(), 8 * 2);

    let val: ocl::aliases::ClUshort16 = Default::default();
    assert_eq!(val.get_size(), 16 * 2);
}

#[test]
fn test_vector_string() {
    let val: ocl::aliases::ClChar2 = Default::default();
    assert_eq!(val.get_opencl_type(), "char2");

    let val: ocl::aliases::ClChar3 = Default::default();
    assert_eq!(val.get_opencl_type(), "char3");

    let val: ocl::aliases::ClChar4 = Default::default();
    assert_eq!(val.get_opencl_type(), "char4");

    let val: ocl::aliases::ClChar8 = Default::default();
    assert_eq!(val.get_opencl_type(), "char8");

    let val: ocl::aliases::ClChar16 = Default::default();
    assert_eq!(val.get_opencl_type(), "char16");

    let val: ocl::aliases::ClDouble2 = Default::default();
    assert_eq!(val.get_opencl_type(), "double2");

    let val: ocl::aliases::ClDouble3 = Default::default();
    assert_eq!(val.get_opencl_type(), "double3");

    let val: ocl::aliases::ClDouble4 = Default::default();
    assert_eq!(val.get_opencl_type(), "double4");

    let val: ocl::aliases::ClDouble8 = Default::default();
    assert_eq!(val.get_opencl_type(), "double8");

    let val: ocl::aliases::ClDouble16 = Default::default();
    assert_eq!(val.get_opencl_type(), "double16");

    let val: ocl::aliases::ClFloat2 = Default::default();
    assert_eq!(val.get_opencl_type(), "float2");

    let val: ocl::aliases::ClFloat3 = Default::default();
    assert_eq!(val.get_opencl_type(), "float3");

    let val: ocl::aliases::ClFloat4 = Default::default();
    assert_eq!(val.get_opencl_type(), "float4");

    let val: ocl::aliases::ClFloat8 = Default::default();
    assert_eq!(val.get_opencl_type(), "float8");

    let val: ocl::aliases::ClFloat16 = Default::default();
    assert_eq!(val.get_opencl_type(), "float16");

    let val: ocl::aliases::ClInt2 = Default::default();
    assert_eq!(val.get_opencl_type(), "int2");

    let val: ocl::aliases::ClInt3 = Default::default();
    assert_eq!(val.get_opencl_type(), "int3");

    let val: ocl::aliases::ClInt4 = Default::default();
    assert_eq!(val.get_opencl_type(), "int4");

    let val: ocl::aliases::ClInt8 = Default::default();
    assert_eq!(val.get_opencl_type(), "int8");

    let val: ocl::aliases::ClInt16 = Default::default();
    assert_eq!(val.get_opencl_type(), "int16");

    let val: ocl::aliases::ClLong1 = Default::default();
    assert_eq!(val.get_opencl_type(), "long");

    let val: ocl::aliases::ClLong2 = Default::default();
    assert_eq!(val.get_opencl_type(), "long2");

    let val: ocl::aliases::ClLong3 = Default::default();
    assert_eq!(val.get_opencl_type(), "long3");

    let val: ocl::aliases::ClLong4 = Default::default();
    assert_eq!(val.get_opencl_type(), "long4");

    let val: ocl::aliases::ClLong8 = Default::default();
    assert_eq!(val.get_opencl_type(), "long8");

    let val: ocl::aliases::ClLong16 = Default::default();
    assert_eq!(val.get_opencl_type(), "long16");

    let val: ocl::aliases::ClShort2 = Default::default();
    assert_eq!(val.get_opencl_type(), "short2");

    let val: ocl::aliases::ClShort3 = Default::default();
    assert_eq!(val.get_opencl_type(), "short3");

    let val: ocl::aliases::ClShort4 = Default::default();
    assert_eq!(val.get_opencl_type(), "short4");

    let val: ocl::aliases::ClShort8 = Default::default();
    assert_eq!(val.get_opencl_type(), "short8");

    let val: ocl::aliases::ClShort16 = Default::default();
    assert_eq!(val.get_opencl_type(), "short16");

    let val: ocl::aliases::ClUchar2 = Default::default();
    assert_eq!(val.get_opencl_type(), "uchar2");

    let val: ocl::aliases::ClUchar3 = Default::default();
    assert_eq!(val.get_opencl_type(), "uchar3");

    let val: ocl::aliases::ClUchar4 = Default::default();
    assert_eq!(val.get_opencl_type(), "uchar4");

    let val: ocl::aliases::ClUchar8 = Default::default();
    assert_eq!(val.get_opencl_type(), "uchar8");

    let val: ocl::aliases::ClUchar16 = Default::default();
    assert_eq!(val.get_opencl_type(), "uchar16");

    let val: ocl::aliases::ClUint2 = Default::default();
    assert_eq!(val.get_opencl_type(), "uint2");

    let val: ocl::aliases::ClUint3 = Default::default();
    assert_eq!(val.get_opencl_type(), "uint3");

    let val: ocl::aliases::ClUint4 = Default::default();
    assert_eq!(val.get_opencl_type(), "uint4");

    let val: ocl::aliases::ClUint8 = Default::default();
    assert_eq!(val.get_opencl_type(), "uint8");

    let val: ocl::aliases::ClUint16 = Default::default();
    assert_eq!(val.get_opencl_type(), "uint16");

    let val: ocl::aliases::ClUlong1 = Default::default();
    assert_eq!(val.get_opencl_type(), "ulong");

    let val: ocl::aliases::ClUlong2 = Default::default();
    assert_eq!(val.get_opencl_type(), "ulong2");

    let val: ocl::aliases::ClUlong3 = Default::default();
    assert_eq!(val.get_opencl_type(), "ulong3");

    let val: ocl::aliases::ClUlong4 = Default::default();
    assert_eq!(val.get_opencl_type(), "ulong4");

    let val: ocl::aliases::ClUlong8 = Default::default();
    assert_eq!(val.get_opencl_type(), "ulong8");

    let val: ocl::aliases::ClUlong16 = Default::default();
    assert_eq!(val.get_opencl_type(), "ulong16");

    let val: ocl::aliases::ClUshort2 = Default::default();
    assert_eq!(val.get_opencl_type(), "ushort2");

    let val: ocl::aliases::ClUshort3 = Default::default();
    assert_eq!(val.get_opencl_type(), "ushort3");

    let val: ocl::aliases::ClUshort4 = Default::default();
    assert_eq!(val.get_opencl_type(), "ushort4");

    let val: ocl::aliases::ClUshort8 = Default::default();
    assert_eq!(val.get_opencl_type(), "ushort8");

    let val: ocl::aliases::ClUshort16 = Default::default();
    assert_eq!(val.get_opencl_type(), "ushort16");
}

#[test]
fn test_vector_base_string() {
    let val: ocl::aliases::ClChar2 = Default::default();
    assert_eq!(val.get_base_type(), "char");

    let val: ocl::aliases::ClChar3 = Default::default();
    assert_eq!(val.get_base_type(), "char");

    let val: ocl::aliases::ClChar4 = Default::default();
    assert_eq!(val.get_base_type(), "char");

    let val: ocl::aliases::ClChar8 = Default::default();
    assert_eq!(val.get_base_type(), "char");

    let val: ocl::aliases::ClChar16 = Default::default();
    assert_eq!(val.get_base_type(), "char");

    let val: ocl::aliases::ClDouble2 = Default::default();
    assert_eq!(val.get_base_type(), "double");

    let val: ocl::aliases::ClDouble3 = Default::default();
    assert_eq!(val.get_base_type(), "double");

    let val: ocl::aliases::ClDouble4 = Default::default();
    assert_eq!(val.get_base_type(), "double");

    let val: ocl::aliases::ClDouble8 = Default::default();
    assert_eq!(val.get_base_type(), "double");

    let val: ocl::aliases::ClDouble16 = Default::default();
    assert_eq!(val.get_base_type(), "double");

    let val: ocl::aliases::ClFloat2 = Default::default();
    assert_eq!(val.get_base_type(), "float");

    let val: ocl::aliases::ClFloat3 = Default::default();
    assert_eq!(val.get_base_type(), "float");

    let val: ocl::aliases::ClFloat4 = Default::default();
    assert_eq!(val.get_base_type(), "float");

    let val: ocl::aliases::ClFloat8 = Default::default();
    assert_eq!(val.get_base_type(), "float");

    let val: ocl::aliases::ClFloat16 = Default::default();
    assert_eq!(val.get_base_type(), "float");

    let val: ocl::aliases::ClInt2 = Default::default();
    assert_eq!(val.get_base_type(), "int");

    let val: ocl::aliases::ClInt3 = Default::default();
    assert_eq!(val.get_base_type(), "int");

    let val: ocl::aliases::ClInt4 = Default::default();
    assert_eq!(val.get_base_type(), "int");

    let val: ocl::aliases::ClInt8 = Default::default();
    assert_eq!(val.get_base_type(), "int");

    let val: ocl::aliases::ClInt16 = Default::default();
    assert_eq!(val.get_base_type(), "int");

    let val: ocl::aliases::ClLong1 = Default::default();
    assert_eq!(val.get_base_type(), "long");

    let val: ocl::aliases::ClLong2 = Default::default();
    assert_eq!(val.get_base_type(), "long");

    let val: ocl::aliases::ClLong3 = Default::default();
    assert_eq!(val.get_base_type(), "long");

    let val: ocl::aliases::ClLong4 = Default::default();
    assert_eq!(val.get_base_type(), "long");

    let val: ocl::aliases::ClLong8 = Default::default();
    assert_eq!(val.get_base_type(), "long");

    let val: ocl::aliases::ClLong16 = Default::default();
    assert_eq!(val.get_base_type(), "long");

    let val: ocl::aliases::ClShort2 = Default::default();
    assert_eq!(val.get_base_type(), "short");

    let val: ocl::aliases::ClShort3 = Default::default();
    assert_eq!(val.get_base_type(), "short");

    let val: ocl::aliases::ClShort4 = Default::default();
    assert_eq!(val.get_base_type(), "short");

    let val: ocl::aliases::ClShort8 = Default::default();
    assert_eq!(val.get_base_type(), "short");

    let val: ocl::aliases::ClShort16 = Default::default();
    assert_eq!(val.get_base_type(), "short");

    let val: ocl::aliases::ClUchar2 = Default::default();
    assert_eq!(val.get_base_type(), "uchar");

    let val: ocl::aliases::ClUchar3 = Default::default();
    assert_eq!(val.get_base_type(), "uchar");

    let val: ocl::aliases::ClUchar4 = Default::default();
    assert_eq!(val.get_base_type(), "uchar");

    let val: ocl::aliases::ClUchar8 = Default::default();
    assert_eq!(val.get_base_type(), "uchar");

    let val: ocl::aliases::ClUchar16 = Default::default();
    assert_eq!(val.get_base_type(), "uchar");

    let val: ocl::aliases::ClUint2 = Default::default();
    assert_eq!(val.get_base_type(), "uint");

    let val: ocl::aliases::ClUint3 = Default::default();
    assert_eq!(val.get_base_type(), "uint");

    let val: ocl::aliases::ClUint4 = Default::default();
    assert_eq!(val.get_base_type(), "uint");

    let val: ocl::aliases::ClUint8 = Default::default();
    assert_eq!(val.get_base_type(), "uint");

    let val: ocl::aliases::ClUint16 = Default::default();
    assert_eq!(val.get_base_type(), "uint");

    let val: ocl::aliases::ClUlong1 = Default::default();
    assert_eq!(val.get_base_type(), "ulong");

    let val: ocl::aliases::ClUlong2 = Default::default();
    assert_eq!(val.get_base_type(), "ulong");

    let val: ocl::aliases::ClUlong3 = Default::default();
    assert_eq!(val.get_base_type(), "ulong");

    let val: ocl::aliases::ClUlong4 = Default::default();
    assert_eq!(val.get_base_type(), "ulong");

    let val: ocl::aliases::ClUlong8 = Default::default();
    assert_eq!(val.get_base_type(), "ulong");

    let val: ocl::aliases::ClUlong16 = Default::default();
    assert_eq!(val.get_base_type(), "ulong");

    let val: ocl::aliases::ClUshort2 = Default::default();
    assert_eq!(val.get_base_type(), "ushort");

    let val: ocl::aliases::ClUshort3 = Default::default();
    assert_eq!(val.get_base_type(), "ushort");

    let val: ocl::aliases::ClUshort4 = Default::default();
    assert_eq!(val.get_base_type(), "ushort");

    let val: ocl::aliases::ClUshort8 = Default::default();
    assert_eq!(val.get_base_type(), "ushort");

    let val: ocl::aliases::ClUshort16 = Default::default();
    assert_eq!(val.get_base_type(), "ushort");
}

#[test]
fn test_scalar_width() {
    assert_eq!(1u8.get_width(), 1);
    assert_eq!(1u16.get_width(), 1);
    assert_eq!(1i8.get_width(), 1);
    assert_eq!(1i16.get_width(), 1);
    assert_eq!(1u32.get_width(), 1);
    assert_eq!(1u64.get_width(), 1);
    assert_eq!(1i32.get_width(), 1);
    assert_eq!(1i64.get_width(), 1);
    assert_eq!(1f32.get_width(), 1);
    assert_eq!(1f64.get_width(), 1);
    assert_eq!(1usize.get_width(), 1);
    assert_eq!(1isize.get_width(), 1);
}

#[test]
fn test_scalar_size() {
    assert_eq!(1u8.get_size(), 1);
    assert_eq!(1u16.get_size(), 2);
    assert_eq!(1i8.get_size(), 1);
    assert_eq!(1i16.get_size(), 2);
    assert_eq!(1u32.get_size(), 4);
    assert_eq!(1u64.get_size(), 8);
    assert_eq!(1i32.get_size(), 4);
    assert_eq!(1i64.get_size(), 8);
    assert_eq!(1f32.get_size(), 4);
    assert_eq!(1f64.get_size(), 8);
    assert_eq!(1usize.get_size(), 8);
    assert_eq!(1isize.get_size(), 8);
}

#[test]
fn test_scalar_string() {
    assert_eq!(1u8.get_opencl_type(), "uchar".to_owned());
    assert_eq!(1u16.get_opencl_type(), "ushort".to_owned());
    assert_eq!(1i8.get_opencl_type(), "char".to_owned());
    assert_eq!(1i16.get_opencl_type(), "short".to_owned());
    assert_eq!(1u32.get_opencl_type(), "uint".to_owned());
    assert_eq!(1u64.get_opencl_type(), "ulong".to_owned());
    assert_eq!(1i32.get_opencl_type(), "int".to_owned());
    assert_eq!(1i64.get_opencl_type(), "long".to_owned());
    assert_eq!(1f32.get_opencl_type(), "float".to_owned());
    assert_eq!(1f64.get_opencl_type(), "double".to_owned());
    assert_eq!(1usize.get_opencl_type(), "size".to_owned());
    assert_eq!(1isize.get_opencl_type(), "size".to_owned());
}
