If you have an OpenCL micro-benchmark that should be part of the suite, create an issue. A better idea is to create a merge request (pull request in GitHub parlance) with your idea. 

Also have a look at the open issues before creating an issue.

Merge requests solving open issues are very welcome!

Please run [rustfmt](https://github.com/rust-lang-nursery/rustfmt) on your file before you create the merge request.
